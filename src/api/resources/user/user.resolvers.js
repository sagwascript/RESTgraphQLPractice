import { User } from './user.model'
import { Playlist } from '../playlist/playlist.model'

const getMe = (_, __, { user }) => {
  return user
}

const getAll = () => {
  return User.find({}).exec()
}

const updateMe = (_, { input }, { user }) => {
  return User.findByIdAndUpdate(user.id, input, { new: true }).exec()
}

export const userResolvers = {
  Query: {
    getMe,
    getAll
  },
  Mutation: {
    updateMe
  },
  User: {
    playlist({ user }) {
      return Playlist.find({}).exec()
    }
  }
}
