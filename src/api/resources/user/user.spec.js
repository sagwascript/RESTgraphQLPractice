// import createApiSpec from '~/apiSpec'
import { expect } from 'chai'
import { beforeEach, afterEach, describe, it } from 'mocha'
import { dropDb, runQuery, createDummyUser } from '~/testhelpers'

// createApiSpec(User, 'user', { username: 'alex', passwordHash: '123rhja2s' })

describe('Query/Mutation graphQL on User', () => {
  let user

  beforeEach(async () => {
    await dropDb()
    user = await createDummyUser()
    // jwt = signToken(user.id)
  })

  afterEach(async () => {
    await dropDb()
  })

  it('should get all the users', async () => {
    const res = await runQuery(
      `
      {
        getAll {
          id
          username
        }
      }
    `,
      {},
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.getAll).to.be.an('array')
  })

  it('should get user by id', async () => {
    const res = await runQuery(
      `
      {
        getMe {
          id
        }
      }
    `,
      {},
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.getMe).to.be.an('object')
    expect(res.data.getMe.id).to.be.eql(user.id.toString())
  })

  it('should update user', async () => {
    const res = await runQuery(
      `
      mutation updateUser($input: UpdatedUser!) {
        updateMe(input: $input) {
          id
          username
        }
      }
    `,
      { input: { username: 'john' } },
      user
    )

    expect(res.errors).to.not.exist
    expect(res.data.updateMe).to.be.an('object')
    expect(res.data.updateMe.username).to.be.eql('john')
  })
})
