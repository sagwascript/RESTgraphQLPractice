import mongoose from 'mongoose'
import bcrypt from 'bcrypt'

const schema = {
  username: {
    type: String,
    unique: true,
    required: true
  },

  passwordHash: {
    type: String,
    required: true
  }
}

const userSchema = new mongoose.Schema(schema, { timestamps: true })

userSchema.methods = {
  authenticate(plainTextPassword) {
    return bcrypt.compareSync(plainTextPassword, this.password)
  },

  hashPassword(plainTextPassword) {
    if (!plainTextPassword) throw new Error('Could not save the user.')

    const salt = bcrypt.genSaltSync(10)
    return bcrypt.hashSync(plainTextPassword, salt)
  }
}

export const User = mongoose.model('user', userSchema)
