import { generateController } from '../../modules/query'

import { Playlist } from './playlist.model'

export default generateController(Playlist)
