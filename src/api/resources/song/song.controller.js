import { generateController } from '../../modules/query'

import { Song } from './song.model'

export default generateController(Song)
