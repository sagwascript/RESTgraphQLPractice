export const config = {
  expireTime: '30d',
  secrets: {
    JWT_SECRET: 'pr3ttyc00lth0'
  },
  disableAuth: true
}
