import { User } from '../src/api/resources/user/user.model'
import '../src/api/resources/song/song.model'
import '../src/api/resources/playlist/playlist.model'
import { schema } from '../src/api/graphQLRouter'
import mongoose from 'mongoose'
import config from '../src/config'
import { graphql } from 'graphql'

mongoose.Promise = global.Promise

export const removeModel = modelName => {
  const model = mongoose.model(modelName)
  return new Promise((resolve, reject) => {
    if (!model) {
      return resolve()
    }

    model.remove(err => {
      if (err) reject(err)
      else resolve()
    })
  })
}

export const dropDb = () => {
  return mongoose.connect(config.db.url).then(() => Promise.all(mongoose.modelNames().map(removeModel)))
}

export const runQuery = (query, variables, user) => {
  return graphql(schema, query, {}, { user }, variables)
}

export const createDummyUser = () => {
  return User.create({ username: 'stu12', passwordHash: '124bsd2' })
}
