/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading wasm modules
/******/ 	var installedWasmModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// object with all compiled WebAssembly.Modules
/******/ 	__webpack_require__.w = {};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/api/graphQLRouter.js":
/*!**********************************!*\
  !*** ./src/api/graphQLRouter.js ***!
  \**********************************/
/*! exports provided: schema, graphQLRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"schema\", function() { return schema; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"graphQLRouter\", function() { return graphQLRouter; });\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash.merge */ \"lodash.merge\");\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_merge__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! apollo-server-express */ \"apollo-server-express\");\n/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(apollo_server_express__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var graphql_tools__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! graphql-tools */ \"graphql-tools\");\n/* harmony import */ var graphql_tools__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(graphql_tools__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _resources_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resources/user */ \"./src/api/resources/user/index.js\");\n/* harmony import */ var _resources_song__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./resources/song */ \"./src/api/resources/song/index.js\");\n/* harmony import */ var _resources_playlist__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./resources/playlist */ \"./src/api/resources/playlist/index.js\");\n\n\n\n\n\n\n\nvar baseSchema = '\\n  schema{\\n    query: Query,\\n    mutation: Mutation\\n  }\\n';\nvar schema = Object(graphql_tools__WEBPACK_IMPORTED_MODULE_2__[\"makeExecutableSchema\"])({\n  typeDefs: [baseSchema, _resources_user__WEBPACK_IMPORTED_MODULE_3__[\"userType\"], _resources_song__WEBPACK_IMPORTED_MODULE_4__[\"songType\"], _resources_playlist__WEBPACK_IMPORTED_MODULE_5__[\"playlistType\"]],\n  resolvers: lodash_merge__WEBPACK_IMPORTED_MODULE_0___default()({}, _resources_user__WEBPACK_IMPORTED_MODULE_3__[\"userResolvers\"], _resources_song__WEBPACK_IMPORTED_MODULE_4__[\"songResolvers\"], _resources_playlist__WEBPACK_IMPORTED_MODULE_5__[\"playlistResolvers\"])\n});\n\nvar graphQLRouter = Object(apollo_server_express__WEBPACK_IMPORTED_MODULE_1__[\"graphqlExpress\"])(function (req) {\n  return {\n    schema: schema,\n    context: {\n      req: req,\n      user: req.user\n    }\n  };\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL2dyYXBoUUxSb3V0ZXIuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9ncmFwaFFMUm91dGVyLmpzPzUwNDciXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1lcmdlIGZyb20gJ2xvZGFzaC5tZXJnZSdcbmltcG9ydCB7IGdyYXBocWxFeHByZXNzIH0gZnJvbSAnYXBvbGxvLXNlcnZlci1leHByZXNzJ1xuaW1wb3J0IHsgbWFrZUV4ZWN1dGFibGVTY2hlbWEgfSBmcm9tICdncmFwaHFsLXRvb2xzJ1xuaW1wb3J0IHsgdXNlclR5cGUsIHVzZXJSZXNvbHZlcnMgfSBmcm9tICcuL3Jlc291cmNlcy91c2VyJ1xuaW1wb3J0IHsgc29uZ1R5cGUsIHNvbmdSZXNvbHZlcnMgfSBmcm9tICcuL3Jlc291cmNlcy9zb25nJ1xuaW1wb3J0IHsgcGxheWxpc3RUeXBlLCBwbGF5bGlzdFJlc29sdmVycyB9IGZyb20gJy4vcmVzb3VyY2VzL3BsYXlsaXN0J1xuXG5jb25zdCBiYXNlU2NoZW1hID0gYFxuICBzY2hlbWF7XG4gICAgcXVlcnk6IFF1ZXJ5LFxuICAgIG11dGF0aW9uOiBNdXRhdGlvblxuICB9XG5gXG5leHBvcnQgY29uc3Qgc2NoZW1hID0gbWFrZUV4ZWN1dGFibGVTY2hlbWEoe1xuICB0eXBlRGVmczogW2Jhc2VTY2hlbWEsIHVzZXJUeXBlLCBzb25nVHlwZSwgcGxheWxpc3RUeXBlXSxcbiAgcmVzb2x2ZXJzOiBtZXJnZSh7fSwgdXNlclJlc29sdmVycywgc29uZ1Jlc29sdmVycywgcGxheWxpc3RSZXNvbHZlcnMpXG59KVxuXG5leHBvcnQgY29uc3QgZ3JhcGhRTFJvdXRlciA9IGdyYXBocWxFeHByZXNzKHJlcSA9PiAoe1xuICBzY2hlbWEsXG4gIGNvbnRleHQ6IHtcbiAgICByZXEsXG4gICAgdXNlcjogcmVxLnVzZXJcbiAgfVxufSkpXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFNQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFGQTtBQUFBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/graphQLRouter.js\n");

/***/ }),

/***/ "./src/api/index.js":
/*!**************************!*\
  !*** ./src/api/index.js ***!
  \**************************/
/*! exports provided: graphQLRouter, restRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _graphQLRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./graphQLRouter */ \"./src/api/graphQLRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"graphQLRouter\", function() { return _graphQLRouter__WEBPACK_IMPORTED_MODULE_0__[\"graphQLRouter\"]; });\n\n/* harmony import */ var _restRouter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./restRouter */ \"./src/api/restRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"restRouter\", function() { return _restRouter__WEBPACK_IMPORTED_MODULE_1__[\"restRouter\"]; });\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvaW5kZXguanM/YTk1MSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgeyBncmFwaFFMUm91dGVyIH0gZnJvbSAnLi9ncmFwaFFMUm91dGVyJ1xuZXhwb3J0IHsgcmVzdFJvdXRlciB9IGZyb20gJy4vcmVzdFJvdXRlcidcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/index.js\n");

/***/ }),

/***/ "./src/api/modules/auth.js":
/*!*********************************!*\
  !*** ./src/api/modules/auth.js ***!
  \*********************************/
/*! exports provided: signin, decodeToken, getFreshUser, verifyUser, signToken, protect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"signin\", function() { return signin; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"decodeToken\", function() { return decodeToken; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getFreshUser\", function() { return getFreshUser; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verifyUser\", function() { return verifyUser; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"signToken\", function() { return signToken; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"protect\", function() { return protect; });\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/regenerator */ \"babel-runtime/regenerator\");\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ \"babel-runtime/helpers/asyncToGenerator\");\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _resources_user_user_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../resources/user/user.model */ \"./src/api/resources/user/user.model.js\");\n/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jsonwebtoken */ \"jsonwebtoken\");\n/* harmony import */ var jsonwebtoken__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jsonwebtoken__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../config */ \"./src/config/index.js\");\n/* harmony import */ var express_jwt__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! express-jwt */ \"express-jwt\");\n/* harmony import */ var express_jwt__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(express_jwt__WEBPACK_IMPORTED_MODULE_5__);\n\n\n\nvar _this = undefined;\n\n\n\n\n\n\nvar checkToken = express_jwt__WEBPACK_IMPORTED_MODULE_5___default()({ secret: _config__WEBPACK_IMPORTED_MODULE_4__[\"default\"].secrets.JWT_SECRET });\n\nvar signin = function signin(req, res, next) {\n  // req.user will be there from the middleware\n  // verify user. Then we can just create a token\n  // and send it back for the client to consume\n  var token = signToken(req.user.id);\n  res.json({ token: token });\n};\n\nvar decodeToken = function decodeToken() {\n  return function (req, res, next) {\n    if (_config__WEBPACK_IMPORTED_MODULE_4__[\"default\"].disableAuth) {\n      return next();\n    }\n    // make it optional to place token on query string\n    // if it is, place it on the headers where it should be\n    // so checkToken can see it. See follow the 'Bearer 034930493' format\n    // so checkToken can see it and decode it\n    if (req.query && req.query.hasOwnProperty('access_token')) {\n      req.headers.authorization = 'Bearer ' + req.query.access_token;\n    }\n\n    // this will call next if token is valid\n    // and send error if its not. It will attached\n    // the decoded token to req.user\n    checkToken(req, res, next);\n  };\n};\n\nvar getFreshUser = function getFreshUser() {\n  return function () {\n    var _ref = babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(req, res, next) {\n      var user;\n      return babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {\n        while (1) {\n          switch (_context.prev = _context.next) {\n            case 0:\n              if (!_config__WEBPACK_IMPORTED_MODULE_4__[\"default\"].disableAuth) {\n                _context.next = 8;\n                break;\n              }\n\n              _context.next = 3;\n              return _resources_user_user_model__WEBPACK_IMPORTED_MODULE_2__[\"User\"].remove();\n\n            case 3:\n              _context.next = 5;\n              return _resources_user_user_model__WEBPACK_IMPORTED_MODULE_2__[\"User\"].create({ username: 'steve', passwordHash: 'gh12bfs' });\n\n            case 5:\n              user = _context.sent;\n\n              req.user = user;\n              return _context.abrupt('return', next());\n\n            case 8:\n              return _context.abrupt('return', _resources_user_user_model__WEBPACK_IMPORTED_MODULE_2__[\"User\"].findById(req.user.id).then(function (user) {\n                if (!user) {\n                  // if no user is found it was not\n                  // it was a valid JWT but didn't decode\n                  // to a real user in our DB. Either the user was deleted\n                  // since the client got the JWT, or\n                  // it was a JWT from some other source\n                  res.status(401).send('Unauthorized');\n                } else {\n                  // update req.user with fresh user from\n                  // stale token data\n                  req.user = user;\n                  next();\n                }\n              }).catch(function (error) {\n                return next(error);\n              }));\n\n            case 9:\n            case 'end':\n              return _context.stop();\n          }\n        }\n      }, _callee, _this);\n    }));\n\n    return function (_x, _x2, _x3) {\n      return _ref.apply(this, arguments);\n    };\n  }();\n};\n\nvar verifyUser = function verifyUser() {\n  return function (req, res, next) {\n    var username = req.body.username;\n    var password = req.body.password;\n\n    // if no username or password then send\n    if (!username || !password) {\n      res.status(400).send('You need a username and password');\n      return;\n    }\n\n    // look user up in the DB so we can check\n    // if the passwords match for the username\n    _resources_user_user_model__WEBPACK_IMPORTED_MODULE_2__[\"User\"].findOne({ username: username }).then(function (user) {\n      if (!user) {\n        res.status(401).send('No user with the given username');\n      } else {\n        // checking the passowords here\n        if (!user.authenticate(password)) {\n          res.status(401).send('Wrong password');\n        } else {\n          // if everything is good,\n          // then attach to req.user\n          // and call next so the controller\n          // can sign a token from the req.user._id\n          req.user = user;\n          next();\n        }\n      }\n    }).catch(function (error) {\n      return next(err);\n    });\n  };\n};\n\nvar signToken = function signToken(id) {\n  return jsonwebtoken__WEBPACK_IMPORTED_MODULE_3___default.a.sign({ id: id }, _config__WEBPACK_IMPORTED_MODULE_4__[\"default\"].secrets.JWT_SECRET, { expiresIn: _config__WEBPACK_IMPORTED_MODULE_4__[\"default\"].expireTime });\n};\n\nvar protect = [decodeToken(), getFreshUser()];//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL21vZHVsZXMvYXV0aC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL21vZHVsZXMvYXV0aC5qcz85OTc0Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFVzZXIgfSBmcm9tICcuLi9yZXNvdXJjZXMvdXNlci91c2VyLm1vZGVsJ1xuaW1wb3J0IGp3dCBmcm9tICdqc29ud2VidG9rZW4nXG5pbXBvcnQgY29uZmlnIGZyb20gJy4uLy4uL2NvbmZpZydcbmltcG9ydCBleHByZXNzSnd0IGZyb20gJ2V4cHJlc3Mtand0J1xuXG5jb25zdCBjaGVja1Rva2VuID0gZXhwcmVzc0p3dCh7IHNlY3JldDogY29uZmlnLnNlY3JldHMuSldUX1NFQ1JFVCB9KVxuXG5leHBvcnQgY29uc3Qgc2lnbmluID0gKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG4gIC8vIHJlcS51c2VyIHdpbGwgYmUgdGhlcmUgZnJvbSB0aGUgbWlkZGxld2FyZVxuICAvLyB2ZXJpZnkgdXNlci4gVGhlbiB3ZSBjYW4ganVzdCBjcmVhdGUgYSB0b2tlblxuICAvLyBhbmQgc2VuZCBpdCBiYWNrIGZvciB0aGUgY2xpZW50IHRvIGNvbnN1bWVcbiAgY29uc3QgdG9rZW4gPSBzaWduVG9rZW4ocmVxLnVzZXIuaWQpXG4gIHJlcy5qc29uKHsgdG9rZW46IHRva2VuIH0pXG59XG5cbmV4cG9ydCBjb25zdCBkZWNvZGVUb2tlbiA9ICgpID0+IChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICBpZiAoY29uZmlnLmRpc2FibGVBdXRoKSB7XG4gICAgcmV0dXJuIG5leHQoKVxuICB9XG4gIC8vIG1ha2UgaXQgb3B0aW9uYWwgdG8gcGxhY2UgdG9rZW4gb24gcXVlcnkgc3RyaW5nXG4gIC8vIGlmIGl0IGlzLCBwbGFjZSBpdCBvbiB0aGUgaGVhZGVycyB3aGVyZSBpdCBzaG91bGQgYmVcbiAgLy8gc28gY2hlY2tUb2tlbiBjYW4gc2VlIGl0LiBTZWUgZm9sbG93IHRoZSAnQmVhcmVyIDAzNDkzMDQ5MycgZm9ybWF0XG4gIC8vIHNvIGNoZWNrVG9rZW4gY2FuIHNlZSBpdCBhbmQgZGVjb2RlIGl0XG4gIGlmIChyZXEucXVlcnkgJiYgcmVxLnF1ZXJ5Lmhhc093blByb3BlcnR5KCdhY2Nlc3NfdG9rZW4nKSkge1xuICAgIHJlcS5oZWFkZXJzLmF1dGhvcml6YXRpb24gPSAnQmVhcmVyICcgKyByZXEucXVlcnkuYWNjZXNzX3Rva2VuXG4gIH1cblxuICAvLyB0aGlzIHdpbGwgY2FsbCBuZXh0IGlmIHRva2VuIGlzIHZhbGlkXG4gIC8vIGFuZCBzZW5kIGVycm9yIGlmIGl0cyBub3QuIEl0IHdpbGwgYXR0YWNoZWRcbiAgLy8gdGhlIGRlY29kZWQgdG9rZW4gdG8gcmVxLnVzZXJcbiAgY2hlY2tUb2tlbihyZXEsIHJlcywgbmV4dClcbn1cblxuZXhwb3J0IGNvbnN0IGdldEZyZXNoVXNlciA9ICgpID0+IGFzeW5jIChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICBpZiAoY29uZmlnLmRpc2FibGVBdXRoKSB7XG4gICAgYXdhaXQgVXNlci5yZW1vdmUoKVxuICAgIGNvbnN0IHVzZXIgPSBhd2FpdCBVc2VyLmNyZWF0ZSh7IHVzZXJuYW1lOiAnc3RldmUnLCBwYXNzd29yZEhhc2g6ICdnaDEyYmZzJyB9KVxuICAgIHJlcS51c2VyID0gdXNlclxuICAgIHJldHVybiBuZXh0KClcbiAgfVxuXG4gIHJldHVybiBVc2VyLmZpbmRCeUlkKHJlcS51c2VyLmlkKVxuICAgIC50aGVuKGZ1bmN0aW9uKHVzZXIpIHtcbiAgICAgIGlmICghdXNlcikge1xuICAgICAgICAvLyBpZiBubyB1c2VyIGlzIGZvdW5kIGl0IHdhcyBub3RcbiAgICAgICAgLy8gaXQgd2FzIGEgdmFsaWQgSldUIGJ1dCBkaWRuJ3QgZGVjb2RlXG4gICAgICAgIC8vIHRvIGEgcmVhbCB1c2VyIGluIG91ciBEQi4gRWl0aGVyIHRoZSB1c2VyIHdhcyBkZWxldGVkXG4gICAgICAgIC8vIHNpbmNlIHRoZSBjbGllbnQgZ290IHRoZSBKV1QsIG9yXG4gICAgICAgIC8vIGl0IHdhcyBhIEpXVCBmcm9tIHNvbWUgb3RoZXIgc291cmNlXG4gICAgICAgIHJlcy5zdGF0dXMoNDAxKS5zZW5kKCdVbmF1dGhvcml6ZWQnKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gdXBkYXRlIHJlcS51c2VyIHdpdGggZnJlc2ggdXNlciBmcm9tXG4gICAgICAgIC8vIHN0YWxlIHRva2VuIGRhdGFcbiAgICAgICAgcmVxLnVzZXIgPSB1c2VyXG4gICAgICAgIG5leHQoKVxuICAgICAgfVxuICAgIH0pXG4gICAgLmNhdGNoKGVycm9yID0+IG5leHQoZXJyb3IpKVxufVxuXG5leHBvcnQgY29uc3QgdmVyaWZ5VXNlciA9ICgpID0+IChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICBjb25zdCB1c2VybmFtZSA9IHJlcS5ib2R5LnVzZXJuYW1lXG4gIGNvbnN0IHBhc3N3b3JkID0gcmVxLmJvZHkucGFzc3dvcmRcblxuICAvLyBpZiBubyB1c2VybmFtZSBvciBwYXNzd29yZCB0aGVuIHNlbmRcbiAgaWYgKCF1c2VybmFtZSB8fCAhcGFzc3dvcmQpIHtcbiAgICByZXMuc3RhdHVzKDQwMCkuc2VuZCgnWW91IG5lZWQgYSB1c2VybmFtZSBhbmQgcGFzc3dvcmQnKVxuICAgIHJldHVyblxuICB9XG5cbiAgLy8gbG9vayB1c2VyIHVwIGluIHRoZSBEQiBzbyB3ZSBjYW4gY2hlY2tcbiAgLy8gaWYgdGhlIHBhc3N3b3JkcyBtYXRjaCBmb3IgdGhlIHVzZXJuYW1lXG4gIFVzZXIuZmluZE9uZSh7IHVzZXJuYW1lOiB1c2VybmFtZSB9KVxuICAgIC50aGVuKGZ1bmN0aW9uKHVzZXIpIHtcbiAgICAgIGlmICghdXNlcikge1xuICAgICAgICByZXMuc3RhdHVzKDQwMSkuc2VuZCgnTm8gdXNlciB3aXRoIHRoZSBnaXZlbiB1c2VybmFtZScpXG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBjaGVja2luZyB0aGUgcGFzc293b3JkcyBoZXJlXG4gICAgICAgIGlmICghdXNlci5hdXRoZW50aWNhdGUocGFzc3dvcmQpKSB7XG4gICAgICAgICAgcmVzLnN0YXR1cyg0MDEpLnNlbmQoJ1dyb25nIHBhc3N3b3JkJylcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyBpZiBldmVyeXRoaW5nIGlzIGdvb2QsXG4gICAgICAgICAgLy8gdGhlbiBhdHRhY2ggdG8gcmVxLnVzZXJcbiAgICAgICAgICAvLyBhbmQgY2FsbCBuZXh0IHNvIHRoZSBjb250cm9sbGVyXG4gICAgICAgICAgLy8gY2FuIHNpZ24gYSB0b2tlbiBmcm9tIHRoZSByZXEudXNlci5faWRcbiAgICAgICAgICByZXEudXNlciA9IHVzZXJcbiAgICAgICAgICBuZXh0KClcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0pXG4gICAgLmNhdGNoKGVycm9yID0+IG5leHQoZXJyKSlcbn1cblxuZXhwb3J0IGNvbnN0IHNpZ25Ub2tlbiA9IGlkID0+IGp3dC5zaWduKHsgaWQgfSwgY29uZmlnLnNlY3JldHMuSldUX1NFQ1JFVCwgeyBleHBpcmVzSW46IGNvbmZpZy5leHBpcmVUaW1lIH0pXG5cbmV4cG9ydCBjb25zdCBwcm90ZWN0ID0gW2RlY29kZVRva2VuKCksIGdldEZyZXNoVXNlcigpXVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBaEJBO0FBQ0E7QUFpQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBR0E7QUFDQTtBQUFBO0FBSkE7QUFDQTtBQURBO0FBQUE7QUFVQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBekJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUEwQkE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBL0JBO0FBQ0E7QUFnQ0E7QUFBQTtBQUFBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/modules/auth.js\n");

/***/ }),

/***/ "./src/api/modules/errorHandler.js":
/*!*****************************************!*\
  !*** ./src/api/modules/errorHandler.js ***!
  \*****************************************/
/*! exports provided: errorHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"errorHandler\", function() { return errorHandler; });\nvar errorHandler = function errorHandler(err, req, res, next) {\n  console.log(err.stack);\n  return res.status(500).send(err.message || err.toString());\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL21vZHVsZXMvZXJyb3JIYW5kbGVyLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvbW9kdWxlcy9lcnJvckhhbmRsZXIuanM/NDRkMCJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY29uc3QgZXJyb3JIYW5kbGVyID0gKGVyciwgcmVxLCByZXMsIG5leHQpID0+IHtcbiAgY29uc29sZS5sb2coZXJyLnN0YWNrKVxuICByZXR1cm4gcmVzLnN0YXR1cyg1MDApLnNlbmQoZXJyLm1lc3NhZ2UgfHwgZXJyLnRvU3RyaW5nKCkpXG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/modules/errorHandler.js\n");

/***/ }),

/***/ "./src/api/modules/query.js":
/*!**********************************!*\
  !*** ./src/api/modules/query.js ***!
  \**********************************/
/*! exports provided: findByParam, getAll, getOne, createOne, updateOne, deleteOne, generateController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"findByParam\", function() { return findByParam; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getAll\", function() { return getAll; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"getOne\", function() { return getOne; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createOne\", function() { return createOne; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"updateOne\", function() { return updateOne; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"deleteOne\", function() { return deleteOne; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"generateController\", function() { return generateController; });\n/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/extends */ \"babel-runtime/helpers/extends\");\n/* harmony import */ var babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/regenerator */ \"babel-runtime/regenerator\");\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ \"babel-runtime/helpers/asyncToGenerator\");\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! babel-runtime/core-js/promise */ \"babel-runtime/core-js/promise\");\n/* harmony import */ var babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! lodash.merge */ \"lodash.merge\");\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(lodash_merge__WEBPACK_IMPORTED_MODULE_4__);\n\n\n\n\n\nvar _this = undefined;\n\n\n\nvar controller = {\n  findByParam: function findByParam(model, id) {\n    return model.findById(id).exec();\n  },\n  createOne: function createOne(model, body) {\n    return model.create(body);\n  },\n  getAll: function getAll(model) {\n    return model.find({}).exec();\n  },\n  getOne: function getOne(docToGet) {\n    return babel_runtime_core_js_promise__WEBPACK_IMPORTED_MODULE_3___default.a.resolve(docToGet);\n  },\n  updateOne: function updateOne(docToUpdate, update) {\n    lodash_merge__WEBPACK_IMPORTED_MODULE_4___default()(docToUpdate, update);\n    return docToUpdate.save();\n  },\n  deleteOne: function deleteOne(docToDelete) {\n    return docToDelete.remove();\n  }\n};\n\nvar findByParam = function findByParam(model) {\n  return function (req, res, next, id) {\n    return controller.findByParam(model, id).then(function (doc) {\n      if (!doc) {\n        next(new Error('Not Found!'));\n      } else {\n        req.docFromId = doc;\n        next();\n      }\n    }).catch(function (err) {\n      return next(err);\n    });\n  };\n};\n\nvar getAll = function getAll(model) {\n  return function (req, res, next) {\n    return controller.getAll(model).then(function (docs) {\n      return res.json(docs);\n    }).catch(function (err) {\n      return next(err);\n    });\n  };\n};\n\nvar getOne = function getOne() {\n  return function (req, res, next) {\n    return controller.getOne(req.docFromId).then(function (doc) {\n      return res.status(201).json(doc);\n    }).catch(function (err) {\n      return next(err);\n    });\n  };\n};\n\nvar createOne = function createOne(model) {\n  return function (req, res, next) {\n    return controller.createOne(model, req.body).then(function (doc) {\n      return res.status(201).json(doc);\n    }).catch(function (err) {\n      return next(err);\n    });\n  };\n};\n\nvar updateOne = function updateOne() {\n  return function () {\n    var _ref = babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(req, res, next) {\n      var docToUpdate, update;\n      return babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {\n        while (1) {\n          switch (_context.prev = _context.next) {\n            case 0:\n              docToUpdate = req.docFromId;\n              update = req.body;\n              return _context.abrupt('return', controller.updateOne(docToUpdate, update).then(function (doc) {\n                return res.status(201).json(doc);\n              }).catch(function (err) {\n                return next(err);\n              }));\n\n            case 3:\n            case 'end':\n              return _context.stop();\n          }\n        }\n      }, _callee, _this);\n    }));\n\n    return function (_x, _x2, _x3) {\n      return _ref.apply(this, arguments);\n    };\n  }();\n};\n\nvar deleteOne = function deleteOne() {\n  return function (req, res, next) {\n    return controller.deleteOne(req.docFromId).then(function (doc) {\n      return res.status(201).json(doc);\n    }).catch(function (err) {\n      return next(err);\n    });\n  };\n};\n\nvar generateController = function generateController(model) {\n  var overrides = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};\n\n  var defaults = {\n    getAll: getAll(model),\n    getOne: getOne(),\n    createOne: createOne(model),\n    updateOne: updateOne(),\n    deleteOne: deleteOne(),\n    findByParam: findByParam(model)\n  };\n\n  return babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({}, defaults, overrides);\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL21vZHVsZXMvcXVlcnkuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9tb2R1bGVzL3F1ZXJ5LmpzPzU2MDIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1lcmdlIGZyb20gJ2xvZGFzaC5tZXJnZSdcblxuY29uc3QgY29udHJvbGxlciA9IHtcbiAgZmluZEJ5UGFyYW0obW9kZWwsIGlkKSB7XG4gICAgcmV0dXJuIG1vZGVsLmZpbmRCeUlkKGlkKS5leGVjKClcbiAgfSxcblxuICBjcmVhdGVPbmUobW9kZWwsIGJvZHkpIHtcbiAgICByZXR1cm4gbW9kZWwuY3JlYXRlKGJvZHkpXG4gIH0sXG5cbiAgZ2V0QWxsKG1vZGVsKSB7XG4gICAgcmV0dXJuIG1vZGVsLmZpbmQoe30pLmV4ZWMoKVxuICB9LFxuXG4gIGdldE9uZShkb2NUb0dldCkge1xuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoZG9jVG9HZXQpXG4gIH0sXG5cbiAgdXBkYXRlT25lKGRvY1RvVXBkYXRlLCB1cGRhdGUpIHtcbiAgICBtZXJnZShkb2NUb1VwZGF0ZSwgdXBkYXRlKVxuICAgIHJldHVybiBkb2NUb1VwZGF0ZS5zYXZlKClcbiAgfSxcblxuICBkZWxldGVPbmUoZG9jVG9EZWxldGUpIHtcbiAgICByZXR1cm4gZG9jVG9EZWxldGUucmVtb3ZlKClcbiAgfVxufVxuXG5leHBvcnQgY29uc3QgZmluZEJ5UGFyYW0gPSBtb2RlbCA9PiAocmVxLCByZXMsIG5leHQsIGlkKSA9PiB7XG4gIHJldHVybiBjb250cm9sbGVyXG4gICAgLmZpbmRCeVBhcmFtKG1vZGVsLCBpZClcbiAgICAudGhlbihkb2MgPT4ge1xuICAgICAgaWYgKCFkb2MpIHtcbiAgICAgICAgbmV4dChuZXcgRXJyb3IoJ05vdCBGb3VuZCEnKSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJlcS5kb2NGcm9tSWQgPSBkb2NcbiAgICAgICAgbmV4dCgpXG4gICAgICB9XG4gICAgfSlcbiAgICAuY2F0Y2goZXJyID0+IG5leHQoZXJyKSlcbn1cblxuZXhwb3J0IGNvbnN0IGdldEFsbCA9IG1vZGVsID0+IChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICByZXR1cm4gY29udHJvbGxlclxuICAgIC5nZXRBbGwobW9kZWwpXG4gICAgLnRoZW4oZG9jcyA9PiByZXMuanNvbihkb2NzKSlcbiAgICAuY2F0Y2goZXJyID0+IG5leHQoZXJyKSlcbn1cblxuZXhwb3J0IGNvbnN0IGdldE9uZSA9ICgpID0+IChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICByZXR1cm4gY29udHJvbGxlclxuICAgIC5nZXRPbmUocmVxLmRvY0Zyb21JZClcbiAgICAudGhlbihkb2MgPT4gcmVzLnN0YXR1cygyMDEpLmpzb24oZG9jKSlcbiAgICAuY2F0Y2goZXJyID0+IG5leHQoZXJyKSlcbn1cblxuZXhwb3J0IGNvbnN0IGNyZWF0ZU9uZSA9IG1vZGVsID0+IChyZXEsIHJlcywgbmV4dCkgPT4ge1xuICByZXR1cm4gY29udHJvbGxlclxuICAgIC5jcmVhdGVPbmUobW9kZWwsIHJlcS5ib2R5KVxuICAgIC50aGVuKGRvYyA9PiByZXMuc3RhdHVzKDIwMSkuanNvbihkb2MpKVxuICAgIC5jYXRjaChlcnIgPT4gbmV4dChlcnIpKVxufVxuXG5leHBvcnQgY29uc3QgdXBkYXRlT25lID0gKCkgPT4gYXN5bmMgKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG4gIGNvbnN0IGRvY1RvVXBkYXRlID0gcmVxLmRvY0Zyb21JZFxuICBjb25zdCB1cGRhdGUgPSByZXEuYm9keVxuXG4gIHJldHVybiBjb250cm9sbGVyXG4gICAgLnVwZGF0ZU9uZShkb2NUb1VwZGF0ZSwgdXBkYXRlKVxuICAgIC50aGVuKGRvYyA9PiByZXMuc3RhdHVzKDIwMSkuanNvbihkb2MpKVxuICAgIC5jYXRjaChlcnIgPT4gbmV4dChlcnIpKVxufVxuXG5leHBvcnQgY29uc3QgZGVsZXRlT25lID0gKCkgPT4gKHJlcSwgcmVzLCBuZXh0KSA9PiB7XG4gIHJldHVybiBjb250cm9sbGVyXG4gICAgLmRlbGV0ZU9uZShyZXEuZG9jRnJvbUlkKVxuICAgIC50aGVuKGRvYyA9PiByZXMuc3RhdHVzKDIwMSkuanNvbihkb2MpKVxuICAgIC5jYXRjaChlcnIgPT4gbmV4dChlcnIpKVxufVxuXG5leHBvcnQgY29uc3QgZ2VuZXJhdGVDb250cm9sbGVyID0gKG1vZGVsLCBvdmVycmlkZXMgPSB7fSkgPT4ge1xuICBjb25zdCBkZWZhdWx0cyA9IHtcbiAgICBnZXRBbGw6IGdldEFsbChtb2RlbCksXG4gICAgZ2V0T25lOiBnZXRPbmUoKSxcbiAgICBjcmVhdGVPbmU6IGNyZWF0ZU9uZShtb2RlbCksXG4gICAgdXBkYXRlT25lOiB1cGRhdGVPbmUoKSxcbiAgICBkZWxldGVPbmU6IGRlbGV0ZU9uZSgpLFxuICAgIGZpbmRCeVBhcmFtOiBmaW5kQnlQYXJhbShtb2RlbClcbiAgfVxuXG4gIHJldHVybiB7IC4uLmRlZmF1bHRzLCAuLi5vdmVycmlkZXMgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUF4QkE7QUFDQTtBQTBCQTtBQUFBO0FBQ0E7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQUE7QUFDQTtBQVpBO0FBQ0E7QUFhQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBTEE7QUFDQTtBQU1BO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFMQTtBQUNBO0FBTUE7QUFBQTtBQUNBO0FBRUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUxBO0FBQ0E7QUFNQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUZBO0FBTUE7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQVJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFTQTtBQUFBO0FBQ0E7QUFFQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBTEE7QUFDQTtBQU1BO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBTkE7QUFDQTtBQVFBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/modules/query.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/index.js":
/*!*********************************************!*\
  !*** ./src/api/resources/playlist/index.js ***!
  \*********************************************/
/*! exports provided: playlistRouter, playlistType, playlistResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _playlist_restRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./playlist.restRouter */ \"./src/api/resources/playlist/playlist.restRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"playlistRouter\", function() { return _playlist_restRouter__WEBPACK_IMPORTED_MODULE_0__[\"playlistRouter\"]; });\n\n/* harmony import */ var _playlist_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playlist.graphQLRouter */ \"./src/api/resources/playlist/playlist.graphQLRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"playlistType\", function() { return _playlist_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"playlistType\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"playlistResolvers\", function() { return _playlist_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"playlistResolvers\"]; });\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9pbmRleC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9pbmRleC5qcz9kY2IxIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vcGxheWxpc3QucmVzdFJvdXRlcidcbmV4cG9ydCAqIGZyb20gJy4vcGxheWxpc3QuZ3JhcGhRTFJvdXRlcidcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/index.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.controller.js":
/*!***********************************************************!*\
  !*** ./src/api/resources/playlist/playlist.controller.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../modules/query */ \"./src/api/modules/query.js\");\n/* harmony import */ var _playlist_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playlist.model */ \"./src/api/resources/playlist/playlist.model.js\");\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(_modules_query__WEBPACK_IMPORTED_MODULE_0__[\"generateController\"])(_playlist_model__WEBPACK_IMPORTED_MODULE_1__[\"Playlist\"]));//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5jb250cm9sbGVyLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3BsYXlsaXN0L3BsYXlsaXN0LmNvbnRyb2xsZXIuanM/NzllMiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBnZW5lcmF0ZUNvbnRyb2xsZXIgfSBmcm9tICcuLi8uLi9tb2R1bGVzL3F1ZXJ5J1xuXG5pbXBvcnQgeyBQbGF5bGlzdCB9IGZyb20gJy4vcGxheWxpc3QubW9kZWwnXG5cbmV4cG9ydCBkZWZhdWx0IGdlbmVyYXRlQ29udHJvbGxlcihQbGF5bGlzdClcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.controller.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.graphQLRouter.js":
/*!**************************************************************!*\
  !*** ./src/api/resources/playlist/playlist.graphQLRouter.js ***!
  \**************************************************************/
/*! exports provided: playlistType, playlistResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _playlist_graphql__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./playlist.graphql */ \"./src/api/resources/playlist/playlist.graphql\");\n/* harmony import */ var _playlist_graphql__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_playlist_graphql__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, \"playlistType\", function() { return _playlist_graphql__WEBPACK_IMPORTED_MODULE_0__; });\n/* harmony import */ var _playlist_resolvers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playlist.resolvers */ \"./src/api/resources/playlist/playlist.resolvers.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"playlistResolvers\", function() { return _playlist_resolvers__WEBPACK_IMPORTED_MODULE_1__[\"playlistResolvers\"]; });\n\n\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5ncmFwaFFMUm91dGVyLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3BsYXlsaXN0L3BsYXlsaXN0LmdyYXBoUUxSb3V0ZXIuanM/MzlkNSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBhcyBwbGF5bGlzdFR5cGUgZnJvbSAnLi9wbGF5bGlzdC5ncmFwaHFsJ1xuZXhwb3J0IHsgcGxheWxpc3RSZXNvbHZlcnMgfSBmcm9tICcuL3BsYXlsaXN0LnJlc29sdmVycydcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7Iiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.graphQLRouter.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.graphql":
/*!*****************************************************!*\
  !*** ./src/api/resources/playlist/playlist.graphql ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"type Playlist {\\n    id: ID!\\n    title: String!\\n    songs: [Song]!\\n    favorite: Boolean!\\n}\\n\\ninput NewPlaylist {\\n    title: String!\\n    songs: [ID]!\\n    favorite: Boolean!\\n}\\n\\ninput UpdatedPlaylist {\\n    id: ID!\\n    title: String\\n    songs: [ID]!\\n    favorite: Boolean\\n}\\n\\nextend type Query {\\n    allPlaylists: [Playlist]!,\\n    Playlist(id: ID!): Playlist!\\n}\\n\\nextend type Mutation {\\n    createPlaylist(input: NewPlaylist!): Playlist!\\n    updatePlaylist(input: UpdatedPlaylist!): Playlist!\\n    deletePlaylist(id: ID!): Playlist!\\n}\"//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5ncmFwaHFsLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL2FwaS9yZXNvdXJjZXMvcGxheWxpc3QvcGxheWxpc3QuZ3JhcGhxbD9iYjA3Il0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gXCJ0eXBlIFBsYXlsaXN0IHtcXG4gICAgaWQ6IElEIVxcbiAgICB0aXRsZTogU3RyaW5nIVxcbiAgICBzb25nczogW1NvbmddIVxcbiAgICBmYXZvcml0ZTogQm9vbGVhbiFcXG59XFxuXFxuaW5wdXQgTmV3UGxheWxpc3Qge1xcbiAgICB0aXRsZTogU3RyaW5nIVxcbiAgICBzb25nczogW0lEXSFcXG4gICAgZmF2b3JpdGU6IEJvb2xlYW4hXFxufVxcblxcbmlucHV0IFVwZGF0ZWRQbGF5bGlzdCB7XFxuICAgIGlkOiBJRCFcXG4gICAgdGl0bGU6IFN0cmluZ1xcbiAgICBzb25nczogW0lEXSFcXG4gICAgZmF2b3JpdGU6IEJvb2xlYW5cXG59XFxuXFxuZXh0ZW5kIHR5cGUgUXVlcnkge1xcbiAgICBhbGxQbGF5bGlzdHM6IFtQbGF5bGlzdF0hLFxcbiAgICBQbGF5bGlzdChpZDogSUQhKTogUGxheWxpc3QhXFxufVxcblxcbmV4dGVuZCB0eXBlIE11dGF0aW9uIHtcXG4gICAgY3JlYXRlUGxheWxpc3QoaW5wdXQ6IE5ld1BsYXlsaXN0ISk6IFBsYXlsaXN0IVxcbiAgICB1cGRhdGVQbGF5bGlzdChpbnB1dDogVXBkYXRlZFBsYXlsaXN0ISk6IFBsYXlsaXN0IVxcbiAgICBkZWxldGVQbGF5bGlzdChpZDogSUQhKTogUGxheWxpc3QhXFxufVwiIl0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.graphql\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.model.js":
/*!******************************************************!*\
  !*** ./src/api/resources/playlist/playlist.model.js ***!
  \******************************************************/
/*! exports provided: Playlist */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Playlist\", function() { return Playlist; });\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ \"mongoose\");\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar schema = {\n  title: {\n    type: String,\n    required: [true, 'Playlist must have a title.']\n  },\n\n  songs: [{\n    type: mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema.Types.ObjectId,\n    ref: 'song'\n  }],\n\n  favorite: {\n    type: Boolean,\n    required: true,\n    default: false\n  }\n};\n\nvar playlistSchema = new mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema(schema);\n\nvar Playlist = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model('playlist', playlistSchema);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5tb2RlbC5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5tb2RlbC5qcz83ODI3Il0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBtb25nb29zZSBmcm9tICdtb25nb29zZSdcblxuY29uc3Qgc2NoZW1hID0ge1xuICB0aXRsZToge1xuICAgIHR5cGU6IFN0cmluZyxcbiAgICByZXF1aXJlZDogW3RydWUsICdQbGF5bGlzdCBtdXN0IGhhdmUgYSB0aXRsZS4nXVxuICB9LFxuXG4gIHNvbmdzOiBbXG4gICAge1xuICAgICAgdHlwZTogbW9uZ29vc2UuU2NoZW1hLlR5cGVzLk9iamVjdElkLFxuICAgICAgcmVmOiAnc29uZydcbiAgICB9XG4gIF0sXG5cbiAgZmF2b3JpdGU6IHtcbiAgICB0eXBlOiBCb29sZWFuLFxuICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgIGRlZmF1bHQ6IGZhbHNlXG4gIH1cbn1cblxuY29uc3QgcGxheWxpc3RTY2hlbWEgPSBuZXcgbW9uZ29vc2UuU2NoZW1hKHNjaGVtYSlcblxuZXhwb3J0IGNvbnN0IFBsYXlsaXN0ID0gbW9uZ29vc2UubW9kZWwoJ3BsYXlsaXN0JywgcGxheWxpc3RTY2hlbWEpXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFDQTtBQUlBO0FBRUE7QUFDQTtBQUZBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBYkE7QUFDQTtBQW1CQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.model.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.resolvers.js":
/*!**********************************************************!*\
  !*** ./src/api/resources/playlist/playlist.resolvers.js ***!
  \**********************************************************/
/*! exports provided: playlistResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"playlistResolvers\", function() { return playlistResolvers; });\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/regenerator */ \"babel-runtime/regenerator\");\n/* harmony import */ var babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ \"babel-runtime/helpers/asyncToGenerator\");\n/* harmony import */ var babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ \"babel-runtime/helpers/objectWithoutProperties\");\n/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _playlist_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./playlist.model */ \"./src/api/resources/playlist/playlist.model.js\");\n\n\n\n\nvar _this = undefined;\n\n\n\nvar allPlaylists = function allPlaylists() {\n  return _playlist_model__WEBPACK_IMPORTED_MODULE_3__[\"Playlist\"].find({}).exec();\n};\n\nvar getOne = function getOne(_, _ref) {\n  var id = _ref.id;\n\n  return _playlist_model__WEBPACK_IMPORTED_MODULE_3__[\"Playlist\"].findById(id).exec();\n};\n\nvar createPlaylist = function createPlaylist(_, _ref2) {\n  var input = _ref2.input;\n\n  return _playlist_model__WEBPACK_IMPORTED_MODULE_3__[\"Playlist\"].create(input);\n};\n\nvar updatePlaylist = function updatePlaylist(_, _ref3) {\n  var input = _ref3.input;\n\n  var id = input.id,\n      update = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_2___default()(input, ['id']);\n\n  return _playlist_model__WEBPACK_IMPORTED_MODULE_3__[\"Playlist\"].findByIdAndUpdate(id, update, { new: true });\n};\n\nvar deletePlaylist = function deletePlaylist(_, _ref4) {\n  var id = _ref4.id;\n\n  return _playlist_model__WEBPACK_IMPORTED_MODULE_3__[\"Playlist\"].findByIdAndRemove(id);\n};\n\nvar populateSong = function () {\n  var _ref5 = babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(playlist) {\n    var res;\n    return babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {\n      while (1) {\n        switch (_context.prev = _context.next) {\n          case 0:\n            _context.next = 2;\n            return playlist.populate('songs').execPopulate();\n\n          case 2:\n            res = _context.sent;\n            return _context.abrupt('return', res.songs);\n\n          case 4:\n          case 'end':\n            return _context.stop();\n        }\n      }\n    }, _callee, _this);\n  }));\n\n  return function populateSong(_x) {\n    return _ref5.apply(this, arguments);\n  };\n}();\n\nvar playlistResolvers = {\n  Query: {\n    allPlaylists: allPlaylists,\n    Playlist: getOne\n  },\n  Mutation: {\n    createPlaylist: createPlaylist,\n    updatePlaylist: updatePlaylist,\n    deletePlaylist: deletePlaylist\n  },\n  Playlist: {\n    songs: populateSong\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5yZXNvbHZlcnMuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9yZXNvdXJjZXMvcGxheWxpc3QvcGxheWxpc3QucmVzb2x2ZXJzLmpzPzZiNTgiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGxheWxpc3QgfSBmcm9tICcuL3BsYXlsaXN0Lm1vZGVsJ1xuXG5jb25zdCBhbGxQbGF5bGlzdHMgPSAoKSA9PiB7XG4gIHJldHVybiBQbGF5bGlzdC5maW5kKHt9KS5leGVjKClcbn1cblxuY29uc3QgZ2V0T25lID0gKF8sIHsgaWQgfSkgPT4ge1xuICByZXR1cm4gUGxheWxpc3QuZmluZEJ5SWQoaWQpLmV4ZWMoKVxufVxuXG5jb25zdCBjcmVhdGVQbGF5bGlzdCA9IChfLCB7IGlucHV0IH0pID0+IHtcbiAgcmV0dXJuIFBsYXlsaXN0LmNyZWF0ZShpbnB1dClcbn1cblxuY29uc3QgdXBkYXRlUGxheWxpc3QgPSAoXywgeyBpbnB1dCB9KSA9PiB7XG4gIGNvbnN0IHsgaWQsIC4uLnVwZGF0ZSB9ID0gaW5wdXRcbiAgcmV0dXJuIFBsYXlsaXN0LmZpbmRCeUlkQW5kVXBkYXRlKGlkLCB1cGRhdGUsIHsgbmV3OiB0cnVlIH0pXG59XG5cbmNvbnN0IGRlbGV0ZVBsYXlsaXN0ID0gKF8sIHsgaWQgfSkgPT4ge1xuICByZXR1cm4gUGxheWxpc3QuZmluZEJ5SWRBbmRSZW1vdmUoaWQpXG59XG5cbmNvbnN0IHBvcHVsYXRlU29uZyA9IGFzeW5jIHBsYXlsaXN0ID0+IHtcbiAgY29uc3QgcmVzID0gYXdhaXQgcGxheWxpc3QucG9wdWxhdGUoJ3NvbmdzJykuZXhlY1BvcHVsYXRlKClcbiAgcmV0dXJuIHJlcy5zb25nc1xufVxuXG5leHBvcnQgY29uc3QgcGxheWxpc3RSZXNvbHZlcnMgPSB7XG4gIFF1ZXJ5OiB7XG4gICAgYWxsUGxheWxpc3RzLFxuICAgIFBsYXlsaXN0OiBnZXRPbmVcbiAgfSxcbiAgTXV0YXRpb246IHtcbiAgICBjcmVhdGVQbGF5bGlzdCxcbiAgICB1cGRhdGVQbGF5bGlzdCxcbiAgICBkZWxldGVQbGF5bGlzdFxuICB9LFxuICBQbGF5bGlzdDoge1xuICAgIHNvbmdzOiBwb3B1bGF0ZVNvbmdcbiAgfVxufVxuIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFEQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQ0E7QUFEQTtBQUNBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFJQTtBQUNBO0FBQ0E7QUFDQTtBQUZBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUtBO0FBQ0E7QUFEQTtBQVZBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.resolvers.js\n");

/***/ }),

/***/ "./src/api/resources/playlist/playlist.restRouter.js":
/*!***********************************************************!*\
  !*** ./src/api/resources/playlist/playlist.restRouter.js ***!
  \***********************************************************/
/*! exports provided: playlistRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"playlistRouter\", function() { return playlistRouter; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _playlist_controller__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playlist.controller */ \"./src/api/resources/playlist/playlist.controller.js\");\n\n\n\nvar playlistRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();\n\nplaylistRouter.param('id', _playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].findByParam);\n\nplaylistRouter.route('/').get(_playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getAll).post(_playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].createOne);\n\nplaylistRouter.route('/:id').get(_playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getOne).put(_playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].updateOne).post(_playlist_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].deleteOne);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9wbGF5bGlzdC9wbGF5bGlzdC5yZXN0Um91dGVyLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3BsYXlsaXN0L3BsYXlsaXN0LnJlc3RSb3V0ZXIuanM/YzUyNiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZXhwcmVzcyBmcm9tICdleHByZXNzJ1xuaW1wb3J0IHBsYXlsaXN0Q29udHJvbGxlciBmcm9tICcuL3BsYXlsaXN0LmNvbnRyb2xsZXInXG5cbmV4cG9ydCBjb25zdCBwbGF5bGlzdFJvdXRlciA9IGV4cHJlc3MuUm91dGVyKClcblxucGxheWxpc3RSb3V0ZXIucGFyYW0oJ2lkJywgcGxheWxpc3RDb250cm9sbGVyLmZpbmRCeVBhcmFtKVxuXG5wbGF5bGlzdFJvdXRlclxuICAucm91dGUoJy8nKVxuICAuZ2V0KHBsYXlsaXN0Q29udHJvbGxlci5nZXRBbGwpXG4gIC5wb3N0KHBsYXlsaXN0Q29udHJvbGxlci5jcmVhdGVPbmUpXG5cbnBsYXlsaXN0Um91dGVyXG4gIC5yb3V0ZSgnLzppZCcpXG4gIC5nZXQocGxheWxpc3RDb250cm9sbGVyLmdldE9uZSlcbiAgLnB1dChwbGF5bGlzdENvbnRyb2xsZXIudXBkYXRlT25lKVxuICAucG9zdChwbGF5bGlzdENvbnRyb2xsZXIuZGVsZXRlT25lKVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/playlist/playlist.restRouter.js\n");

/***/ }),

/***/ "./src/api/resources/song/index.js":
/*!*****************************************!*\
  !*** ./src/api/resources/song/index.js ***!
  \*****************************************/
/*! exports provided: songRouter, songType, songResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _song_restRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./song.restRouter */ \"./src/api/resources/song/song.restRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"songRouter\", function() { return _song_restRouter__WEBPACK_IMPORTED_MODULE_0__[\"songRouter\"]; });\n\n/* harmony import */ var _song_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./song.graphQLRouter */ \"./src/api/resources/song/song.graphQLRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"songType\", function() { return _song_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"songType\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"songResolvers\", function() { return _song_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"songResolvers\"]; });\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3NvbmcvaW5kZXguanM/MWUwOSJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tICcuL3NvbmcucmVzdFJvdXRlcidcbmV4cG9ydCAqIGZyb20gJy4vc29uZy5ncmFwaFFMUm91dGVyJ1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/song/index.js\n");

/***/ }),

/***/ "./src/api/resources/song/song.controller.js":
/*!***************************************************!*\
  !*** ./src/api/resources/song/song.controller.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../modules/query */ \"./src/api/modules/query.js\");\n/* harmony import */ var _song_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./song.model */ \"./src/api/resources/song/song.model.js\");\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(_modules_query__WEBPACK_IMPORTED_MODULE_0__[\"generateController\"])(_song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"]));//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcuY29udHJvbGxlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcuY29udHJvbGxlci5qcz9iNWJmIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGdlbmVyYXRlQ29udHJvbGxlciB9IGZyb20gJy4uLy4uL21vZHVsZXMvcXVlcnknXG5cbmltcG9ydCB7IFNvbmcgfSBmcm9tICcuL3NvbmcubW9kZWwnXG5cbmV4cG9ydCBkZWZhdWx0IGdlbmVyYXRlQ29udHJvbGxlcihTb25nKVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.controller.js\n");

/***/ }),

/***/ "./src/api/resources/song/song.graphQLRouter.js":
/*!******************************************************!*\
  !*** ./src/api/resources/song/song.graphQLRouter.js ***!
  \******************************************************/
/*! exports provided: songType, songResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _song_graphql__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./song.graphql */ \"./src/api/resources/song/song.graphql\");\n/* harmony import */ var _song_graphql__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_song_graphql__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, \"songType\", function() { return _song_graphql__WEBPACK_IMPORTED_MODULE_0__; });\n/* harmony import */ var _song_resolvers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./song.resolvers */ \"./src/api/resources/song/song.resolvers.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"songResolvers\", function() { return _song_resolvers__WEBPACK_IMPORTED_MODULE_1__[\"songResolvers\"]; });\n\n\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcuZ3JhcGhRTFJvdXRlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcuZ3JhcGhRTFJvdXRlci5qcz9iNmUxIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGFzIHNvbmdUeXBlIGZyb20gJy4vc29uZy5ncmFwaHFsJ1xuZXhwb3J0IHsgc29uZ1Jlc29sdmVycyB9IGZyb20gJy4vc29uZy5yZXNvbHZlcnMnXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.graphQLRouter.js\n");

/***/ }),

/***/ "./src/api/resources/song/song.graphql":
/*!*********************************************!*\
  !*** ./src/api/resources/song/song.graphql ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"type Song {\\n    id: ID!\\n    title: String!\\n    url: String!\\n    album: String\\n    artist: String\\n    rating: Int!\\n    favorite: Boolean!\\n}\\n\\ninput UpdatedSong {\\n    id: ID!\\n    title: String\\n    url: String\\n    album: String\\n    artist: String\\n    rating: Int\\n    favorite: Boolean\\n}\\n\\ninput NewSong {\\n    title: String!\\n    url: String!\\n    album: String\\n    artist: String\\n    rating: Int\\n    favorite: Boolean\\n}\\n\\nextend type Query {\\n    allSongs: [Song]!\\n    Song(id: ID!): Song!\\n}\\n\\nextend type Mutation {\\n    createSong(input: NewSong!): Song!\\n    updateSong(input: UpdatedSong!): Song!\\n    deleteSong(id: ID!): Song!\\n}\"//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcuZ3JhcGhxbC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9hcGkvcmVzb3VyY2VzL3Nvbmcvc29uZy5ncmFwaHFsPzYyYzkiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSBcInR5cGUgU29uZyB7XFxuICAgIGlkOiBJRCFcXG4gICAgdGl0bGU6IFN0cmluZyFcXG4gICAgdXJsOiBTdHJpbmchXFxuICAgIGFsYnVtOiBTdHJpbmdcXG4gICAgYXJ0aXN0OiBTdHJpbmdcXG4gICAgcmF0aW5nOiBJbnQhXFxuICAgIGZhdm9yaXRlOiBCb29sZWFuIVxcbn1cXG5cXG5pbnB1dCBVcGRhdGVkU29uZyB7XFxuICAgIGlkOiBJRCFcXG4gICAgdGl0bGU6IFN0cmluZ1xcbiAgICB1cmw6IFN0cmluZ1xcbiAgICBhbGJ1bTogU3RyaW5nXFxuICAgIGFydGlzdDogU3RyaW5nXFxuICAgIHJhdGluZzogSW50XFxuICAgIGZhdm9yaXRlOiBCb29sZWFuXFxufVxcblxcbmlucHV0IE5ld1Nvbmcge1xcbiAgICB0aXRsZTogU3RyaW5nIVxcbiAgICB1cmw6IFN0cmluZyFcXG4gICAgYWxidW06IFN0cmluZ1xcbiAgICBhcnRpc3Q6IFN0cmluZ1xcbiAgICByYXRpbmc6IEludFxcbiAgICBmYXZvcml0ZTogQm9vbGVhblxcbn1cXG5cXG5leHRlbmQgdHlwZSBRdWVyeSB7XFxuICAgIGFsbFNvbmdzOiBbU29uZ10hXFxuICAgIFNvbmcoaWQ6IElEISk6IFNvbmchXFxufVxcblxcbmV4dGVuZCB0eXBlIE11dGF0aW9uIHtcXG4gICAgY3JlYXRlU29uZyhpbnB1dDogTmV3U29uZyEpOiBTb25nIVxcbiAgICB1cGRhdGVTb25nKGlucHV0OiBVcGRhdGVkU29uZyEpOiBTb25nIVxcbiAgICBkZWxldGVTb25nKGlkOiBJRCEpOiBTb25nIVxcbn1cIiJdLCJtYXBwaW5ncyI6IkFBQUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.graphql\n");

/***/ }),

/***/ "./src/api/resources/song/song.model.js":
/*!**********************************************!*\
  !*** ./src/api/resources/song/song.model.js ***!
  \**********************************************/
/*! exports provided: Song */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Song\", function() { return Song; });\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ \"mongoose\");\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar schema = {\n  title: {\n    type: String,\n    required: [true, 'Song must have a title.']\n  },\n\n  url: {\n    type: String,\n    unique: true,\n    required: [true, 'Song must have an url']\n  },\n\n  album: String,\n\n  artist: String,\n\n  rating: {\n    type: Number,\n    min: 0,\n    max: 5,\n    default: 0\n  },\n\n  favorite: {\n    type: Boolean,\n    required: true,\n    default: false\n  }\n};\n\nvar songSchema = new mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema(schema);\n\nvar Song = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model('song', songSchema);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcubW9kZWwuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9yZXNvdXJjZXMvc29uZy9zb25nLm1vZGVsLmpzP2E2M2YiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vbmdvb3NlIGZyb20gJ21vbmdvb3NlJ1xuXG5jb25zdCBzY2hlbWEgPSB7XG4gIHRpdGxlOiB7XG4gICAgdHlwZTogU3RyaW5nLFxuICAgIHJlcXVpcmVkOiBbdHJ1ZSwgJ1NvbmcgbXVzdCBoYXZlIGEgdGl0bGUuJ11cbiAgfSxcblxuICB1cmw6IHtcbiAgICB0eXBlOiBTdHJpbmcsXG4gICAgdW5pcXVlOiB0cnVlLFxuICAgIHJlcXVpcmVkOiBbdHJ1ZSwgJ1NvbmcgbXVzdCBoYXZlIGFuIHVybCddXG4gIH0sXG5cbiAgYWxidW06IFN0cmluZyxcblxuICBhcnRpc3Q6IFN0cmluZyxcblxuICByYXRpbmc6IHtcbiAgICB0eXBlOiBOdW1iZXIsXG4gICAgbWluOiAwLFxuICAgIG1heDogNSxcbiAgICBkZWZhdWx0OiAwXG4gIH0sXG5cbiAgZmF2b3JpdGU6IHtcbiAgICB0eXBlOiBCb29sZWFuLFxuICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgIGRlZmF1bHQ6IGZhbHNlXG4gIH1cbn1cblxuY29uc3Qgc29uZ1NjaGVtYSA9IG5ldyBtb25nb29zZS5TY2hlbWEoc2NoZW1hKVxuXG5leHBvcnQgY29uc3QgU29uZyA9IG1vbmdvb3NlLm1vZGVsKCdzb25nJywgc29uZ1NjaGVtYSlcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUNBO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFIQTtBQUNBO0FBS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSkE7QUFDQTtBQU1BO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUF2QkE7QUFDQTtBQTZCQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.model.js\n");

/***/ }),

/***/ "./src/api/resources/song/song.resolvers.js":
/*!**************************************************!*\
  !*** ./src/api/resources/song/song.resolvers.js ***!
  \**************************************************/
/*! exports provided: songResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"songResolvers\", function() { return songResolvers; });\n/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ \"babel-runtime/helpers/objectWithoutProperties\");\n/* harmony import */ var babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _song_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./song.model */ \"./src/api/resources/song/song.model.js\");\n\n\n\nvar allSongs = function allSongs() {\n  return _song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"].find({}).exec();\n};\n\nvar getOne = function getOne(_, _ref) {\n  var id = _ref.id;\n\n  return _song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"].findById(id).exec();\n};\n\nvar createSong = function createSong(_, _ref2) {\n  var input = _ref2.input;\n\n  return _song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"].create(input);\n};\n\nvar updateSong = function updateSong(_, _ref3) {\n  var input = _ref3.input;\n\n  var id = input.id,\n      update = babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default()(input, ['id']);\n\n  return _song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"].findByIdAndUpdate(id, update, { new: true });\n};\n\nvar deleteSong = function deleteSong(_, _ref4) {\n  var id = _ref4.id;\n\n  return _song_model__WEBPACK_IMPORTED_MODULE_1__[\"Song\"].findByIdAndRemove(id);\n};\n\nvar songResolvers = {\n  Query: {\n    allSongs: allSongs,\n    Song: getOne\n  },\n  Mutation: {\n    createSong: createSong,\n    updateSong: updateSong,\n    deleteSong: deleteSong\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcucmVzb2x2ZXJzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3Nvbmcvc29uZy5yZXNvbHZlcnMuanM/Mzk3MiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTb25nIH0gZnJvbSAnLi9zb25nLm1vZGVsJ1xuXG5jb25zdCBhbGxTb25ncyA9ICgpID0+IHtcbiAgcmV0dXJuIFNvbmcuZmluZCh7fSkuZXhlYygpXG59XG5cbmNvbnN0IGdldE9uZSA9IChfLCB7IGlkIH0pID0+IHtcbiAgcmV0dXJuIFNvbmcuZmluZEJ5SWQoaWQpLmV4ZWMoKVxufVxuXG5jb25zdCBjcmVhdGVTb25nID0gKF8sIHsgaW5wdXQgfSkgPT4ge1xuICByZXR1cm4gU29uZy5jcmVhdGUoaW5wdXQpXG59XG5cbmNvbnN0IHVwZGF0ZVNvbmcgPSAoXywgeyBpbnB1dCB9KSA9PiB7XG4gIGNvbnN0IHsgaWQsIC4uLnVwZGF0ZSB9ID0gaW5wdXRcbiAgcmV0dXJuIFNvbmcuZmluZEJ5SWRBbmRVcGRhdGUoaWQsIHVwZGF0ZSwgeyBuZXc6IHRydWUgfSlcbn1cblxuY29uc3QgZGVsZXRlU29uZyA9IChfLCB7IGlkIH0pID0+IHtcbiAgcmV0dXJuIFNvbmcuZmluZEJ5SWRBbmRSZW1vdmUoaWQpXG59XG5cbmV4cG9ydCBjb25zdCBzb25nUmVzb2x2ZXJzID0ge1xuICBRdWVyeToge1xuICAgIGFsbFNvbmdzLFxuICAgIFNvbmc6IGdldE9uZVxuICB9LFxuICBNdXRhdGlvbjoge1xuICAgIGNyZWF0ZVNvbmcsXG4gICAgdXBkYXRlU29uZyxcbiAgICBkZWxldGVTb25nXG4gIH1cbn1cbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBREE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFGQTtBQUlBO0FBQ0E7QUFDQTtBQUNBO0FBSEE7QUFMQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.resolvers.js\n");

/***/ }),

/***/ "./src/api/resources/song/song.restRouter.js":
/*!***************************************************!*\
  !*** ./src/api/resources/song/song.restRouter.js ***!
  \***************************************************/
/*! exports provided: songRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"songRouter\", function() { return songRouter; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _song_controller__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./song.controller */ \"./src/api/resources/song/song.controller.js\");\n\n\n\nvar songRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();\n\nsongRouter.param('id', _song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].findByParam);\n\nsongRouter.route('/').get(_song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getAll).post(_song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].createOne);\n\nsongRouter.route('/:id').get(_song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getOne).put(_song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].updateOne).post(_song_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].deleteOne);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcucmVzdFJvdXRlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy9zb25nL3NvbmcucmVzdFJvdXRlci5qcz9mYTViIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnXG5pbXBvcnQgc29uZ0NvbnRyb2xsZXIgZnJvbSAnLi9zb25nLmNvbnRyb2xsZXInXG5cbmV4cG9ydCBjb25zdCBzb25nUm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKVxuXG5zb25nUm91dGVyLnBhcmFtKCdpZCcsIHNvbmdDb250cm9sbGVyLmZpbmRCeVBhcmFtKVxuXG5zb25nUm91dGVyXG4gIC5yb3V0ZSgnLycpXG4gIC5nZXQoc29uZ0NvbnRyb2xsZXIuZ2V0QWxsKVxuICAucG9zdChzb25nQ29udHJvbGxlci5jcmVhdGVPbmUpXG5cbnNvbmdSb3V0ZXJcbiAgLnJvdXRlKCcvOmlkJylcbiAgLmdldChzb25nQ29udHJvbGxlci5nZXRPbmUpXG4gIC5wdXQoc29uZ0NvbnRyb2xsZXIudXBkYXRlT25lKVxuICAucG9zdChzb25nQ29udHJvbGxlci5kZWxldGVPbmUpXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/song/song.restRouter.js\n");

/***/ }),

/***/ "./src/api/resources/user/index.js":
/*!*****************************************!*\
  !*** ./src/api/resources/user/index.js ***!
  \*****************************************/
/*! exports provided: userRouter, userType, userResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _user_restRouter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.restRouter */ \"./src/api/resources/user/user.restRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"userRouter\", function() { return _user_restRouter__WEBPACK_IMPORTED_MODULE_0__[\"userRouter\"]; });\n\n/* harmony import */ var _user_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.graphQLRouter */ \"./src/api/resources/user/user.graphQLRouter.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"userType\", function() { return _user_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"userType\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"userResolvers\", function() { return _user_graphQLRouter__WEBPACK_IMPORTED_MODULE_1__[\"userResolvers\"]; });\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3VzZXIvaW5kZXguanM/MTBmMCJdLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgKiBmcm9tICcuL3VzZXIucmVzdFJvdXRlcidcbmV4cG9ydCAqIGZyb20gJy4vdXNlci5ncmFwaFFMUm91dGVyJ1xuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTsiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/user/index.js\n");

/***/ }),

/***/ "./src/api/resources/user/user.controller.js":
/*!***************************************************!*\
  !*** ./src/api/resources/user/user.controller.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _modules_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../modules/query */ \"./src/api/modules/query.js\");\n/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.model */ \"./src/api/resources/user/user.model.js\");\n\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Object(_modules_query__WEBPACK_IMPORTED_MODULE_0__[\"generateController\"])(_user_model__WEBPACK_IMPORTED_MODULE_1__[\"User\"]));//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIuY29udHJvbGxlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIuY29udHJvbGxlci5qcz8wMjUyIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IGdlbmVyYXRlQ29udHJvbGxlciB9IGZyb20gJy4uLy4uL21vZHVsZXMvcXVlcnknXG5cbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuL3VzZXIubW9kZWwnXG5cbmV4cG9ydCBkZWZhdWx0IGdlbmVyYXRlQ29udHJvbGxlcihVc2VyKVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.controller.js\n");

/***/ }),

/***/ "./src/api/resources/user/user.graphQLRouter.js":
/*!******************************************************!*\
  !*** ./src/api/resources/user/user.graphQLRouter.js ***!
  \******************************************************/
/*! exports provided: userType, userResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _user_graphql__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.graphql */ \"./src/api/resources/user/user.graphql\");\n/* harmony import */ var _user_graphql__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_user_graphql__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, \"userType\", function() { return _user_graphql__WEBPACK_IMPORTED_MODULE_0__; });\n/* harmony import */ var _user_resolvers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.resolvers */ \"./src/api/resources/user/user.resolvers.js\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"userResolvers\", function() { return _user_resolvers__WEBPACK_IMPORTED_MODULE_1__[\"userResolvers\"]; });\n\n\n\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIuZ3JhcGhRTFJvdXRlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIuZ3JhcGhRTFJvdXRlci5qcz8wYjhhIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGFzIHVzZXJUeXBlIGZyb20gJy4vdXNlci5ncmFwaHFsJ1xuZXhwb3J0IHsgdXNlclJlc29sdmVycyB9IGZyb20gJy4vdXNlci5yZXNvbHZlcnMnXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.graphQLRouter.js\n");

/***/ }),

/***/ "./src/api/resources/user/user.graphql":
/*!*********************************************!*\
  !*** ./src/api/resources/user/user.graphql ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = \"type User {\\n    id: ID!\\n    username: String!\\n    playlist: [Playlist]!\\n    createdAt: String!\\n    updatedAt: String!\\n}\\n\\ninput UpdatedUser {\\n    username: String!\\n}\\n\\ntype Query {\\n    getMe: User!\\n    getAll: [User]!\\n}\\n\\ntype Mutation {\\n    updateMe(input: UpdatedUser!): User!\\n}\"//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIuZ3JhcGhxbC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9hcGkvcmVzb3VyY2VzL3VzZXIvdXNlci5ncmFwaHFsPzYyMDAiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSBcInR5cGUgVXNlciB7XFxuICAgIGlkOiBJRCFcXG4gICAgdXNlcm5hbWU6IFN0cmluZyFcXG4gICAgcGxheWxpc3Q6IFtQbGF5bGlzdF0hXFxuICAgIGNyZWF0ZWRBdDogU3RyaW5nIVxcbiAgICB1cGRhdGVkQXQ6IFN0cmluZyFcXG59XFxuXFxuaW5wdXQgVXBkYXRlZFVzZXIge1xcbiAgICB1c2VybmFtZTogU3RyaW5nIVxcbn1cXG5cXG50eXBlIFF1ZXJ5IHtcXG4gICAgZ2V0TWU6IFVzZXIhXFxuICAgIGdldEFsbDogW1VzZXJdIVxcbn1cXG5cXG50eXBlIE11dGF0aW9uIHtcXG4gICAgdXBkYXRlTWUoaW5wdXQ6IFVwZGF0ZWRVc2VyISk6IFVzZXIhXFxufVwiIl0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.graphql\n");

/***/ }),

/***/ "./src/api/resources/user/user.model.js":
/*!**********************************************!*\
  !*** ./src/api/resources/user/user.model.js ***!
  \**********************************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"User\", function() { return User; });\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ \"mongoose\");\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! bcrypt */ \"bcrypt\");\n/* harmony import */ var bcrypt__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(bcrypt__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nvar schema = {\n  username: {\n    type: String,\n    unique: true,\n    required: true\n  },\n\n  passwordHash: {\n    type: String,\n    required: true\n  }\n};\n\nvar userSchema = new mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Schema(schema, { timestamps: true });\n\nuserSchema.methods = {\n  authenticate: function authenticate(plainTextPassword) {\n    return bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.compareSync(plainTextPassword, this.password);\n  },\n  hashPassword: function hashPassword(plainTextPassword) {\n    if (!plainTextPassword) throw new Error('Could not save the user.');\n\n    var salt = bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.genSaltSync(10);\n    return bcrypt__WEBPACK_IMPORTED_MODULE_1___default.a.hashSync(plainTextPassword, salt);\n  }\n};\n\nvar User = mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.model('user', userSchema);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIubW9kZWwuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9yZXNvdXJjZXMvdXNlci91c2VyLm1vZGVsLmpzPzA5MzIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vbmdvb3NlIGZyb20gJ21vbmdvb3NlJ1xuaW1wb3J0IGJjcnlwdCBmcm9tICdiY3J5cHQnXG5cbmNvbnN0IHNjaGVtYSA9IHtcbiAgdXNlcm5hbWU6IHtcbiAgICB0eXBlOiBTdHJpbmcsXG4gICAgdW5pcXVlOiB0cnVlLFxuICAgIHJlcXVpcmVkOiB0cnVlXG4gIH0sXG5cbiAgcGFzc3dvcmRIYXNoOiB7XG4gICAgdHlwZTogU3RyaW5nLFxuICAgIHJlcXVpcmVkOiB0cnVlXG4gIH1cbn1cblxuY29uc3QgdXNlclNjaGVtYSA9IG5ldyBtb25nb29zZS5TY2hlbWEoc2NoZW1hLCB7IHRpbWVzdGFtcHM6IHRydWUgfSlcblxudXNlclNjaGVtYS5tZXRob2RzID0ge1xuICBhdXRoZW50aWNhdGUocGxhaW5UZXh0UGFzc3dvcmQpIHtcbiAgICByZXR1cm4gYmNyeXB0LmNvbXBhcmVTeW5jKHBsYWluVGV4dFBhc3N3b3JkLCB0aGlzLnBhc3N3b3JkKVxuICB9LFxuXG4gIGhhc2hQYXNzd29yZChwbGFpblRleHRQYXNzd29yZCkge1xuICAgIGlmICghcGxhaW5UZXh0UGFzc3dvcmQpIHRocm93IG5ldyBFcnJvcignQ291bGQgbm90IHNhdmUgdGhlIHVzZXIuJylcblxuICAgIGNvbnN0IHNhbHQgPSBiY3J5cHQuZ2VuU2FsdFN5bmMoMTApXG4gICAgcmV0dXJuIGJjcnlwdC5oYXNoU3luYyhwbGFpblRleHRQYXNzd29yZCwgc2FsdClcbiAgfVxufVxuXG5leHBvcnQgY29uc3QgVXNlciA9IG1vbmdvb3NlLm1vZGVsKCd1c2VyJywgdXNlclNjaGVtYSlcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUhBO0FBQ0E7QUFLQTtBQUNBO0FBQ0E7QUFGQTtBQVBBO0FBQ0E7QUFZQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFWQTtBQUNBO0FBWUEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.model.js\n");

/***/ }),

/***/ "./src/api/resources/user/user.resolvers.js":
/*!**************************************************!*\
  !*** ./src/api/resources/user/user.resolvers.js ***!
  \**************************************************/
/*! exports provided: userResolvers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"userResolvers\", function() { return userResolvers; });\n/* harmony import */ var _user_model__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user.model */ \"./src/api/resources/user/user.model.js\");\n/* harmony import */ var _playlist_playlist_model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../playlist/playlist.model */ \"./src/api/resources/playlist/playlist.model.js\");\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lodash.merge */ \"lodash.merge\");\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_merge__WEBPACK_IMPORTED_MODULE_2__);\n\n\n\n\nvar getMe = function getMe(_, __, _ref) {\n  var user = _ref.user;\n\n  return user;\n};\n\nvar getAll = function getAll() {\n  return _user_model__WEBPACK_IMPORTED_MODULE_0__[\"User\"].find({}).exec();\n};\n\nvar updateMe = function updateMe(_, _ref2, _ref3) {\n  var input = _ref2.input;\n  var user = _ref3.user;\n\n  return _user_model__WEBPACK_IMPORTED_MODULE_0__[\"User\"].findByIdAndUpdate(user.id, input, { new: true }).exec();\n};\n\nvar userResolvers = {\n  Query: {\n    getMe: getMe,\n    getAll: getAll\n  },\n  Mutation: {\n    updateMe: updateMe\n  },\n  User: {\n    playlist: function playlist(_ref4) {\n      var user = _ref4.user;\n\n      return _playlist_playlist_model__WEBPACK_IMPORTED_MODULE_1__[\"Playlist\"].find({}).exec();\n    }\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIucmVzb2x2ZXJzLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9hcGkvcmVzb3VyY2VzL3VzZXIvdXNlci5yZXNvbHZlcnMuanM/OWY2YyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBVc2VyIH0gZnJvbSAnLi91c2VyLm1vZGVsJ1xuaW1wb3J0IHsgUGxheWxpc3QgfSBmcm9tICcuLi9wbGF5bGlzdC9wbGF5bGlzdC5tb2RlbCdcbmltcG9ydCBtZXJnZSBmcm9tICdsb2Rhc2gubWVyZ2UnXG5cbmNvbnN0IGdldE1lID0gKF8sIF9fLCB7IHVzZXIgfSkgPT4ge1xuICByZXR1cm4gdXNlclxufVxuXG5jb25zdCBnZXRBbGwgPSAoKSA9PiB7XG4gIHJldHVybiBVc2VyLmZpbmQoe30pLmV4ZWMoKVxufVxuXG5jb25zdCB1cGRhdGVNZSA9IChfLCB7IGlucHV0IH0sIHsgdXNlciB9KSA9PiB7XG4gIHJldHVybiBVc2VyLmZpbmRCeUlkQW5kVXBkYXRlKHVzZXIuaWQsIGlucHV0LCB7IG5ldzogdHJ1ZSB9KS5leGVjKClcbn1cblxuZXhwb3J0IGNvbnN0IHVzZXJSZXNvbHZlcnMgPSB7XG4gIFF1ZXJ5OiB7XG4gICAgZ2V0TWUsXG4gICAgZ2V0QWxsXG4gIH0sXG4gIE11dGF0aW9uOiB7XG4gICAgdXBkYXRlTWVcbiAgfSxcbiAgVXNlcjoge1xuICAgIHBsYXlsaXN0KHsgdXNlciB9KSB7XG4gICAgICByZXR1cm4gUGxheWxpc3QuZmluZCh7fSkuZXhlYygpXG4gICAgfVxuICB9XG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRkE7QUFJQTtBQUNBO0FBREE7QUFHQTtBQUNBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFIQTtBQVJBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.resolvers.js\n");

/***/ }),

/***/ "./src/api/resources/user/user.restRouter.js":
/*!***************************************************!*\
  !*** ./src/api/resources/user/user.restRouter.js ***!
  \***************************************************/
/*! exports provided: userRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"userRouter\", function() { return userRouter; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _user_controller__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user.controller */ \"./src/api/resources/user/user.controller.js\");\n\n\n\nvar userRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();\n\nuserRouter.param('id', _user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].findByParam);\n\nuserRouter.route('/').get(_user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getAll).post(_user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].createOne);\n\nuserRouter.route('/:id').get(_user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].getOne).put(_user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].updateOne).post(_user_controller__WEBPACK_IMPORTED_MODULE_1__[\"default\"].deleteOne);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIucmVzdFJvdXRlci5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvYXBpL3Jlc291cmNlcy91c2VyL3VzZXIucmVzdFJvdXRlci5qcz85YzQwIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBleHByZXNzIGZyb20gJ2V4cHJlc3MnXG5pbXBvcnQgdXNlckNvbnRyb2xsZXIgZnJvbSAnLi91c2VyLmNvbnRyb2xsZXInXG5cbmV4cG9ydCBjb25zdCB1c2VyUm91dGVyID0gZXhwcmVzcy5Sb3V0ZXIoKVxuXG51c2VyUm91dGVyLnBhcmFtKCdpZCcsIHVzZXJDb250cm9sbGVyLmZpbmRCeVBhcmFtKVxuXG51c2VyUm91dGVyXG4gIC5yb3V0ZSgnLycpXG4gIC5nZXQodXNlckNvbnRyb2xsZXIuZ2V0QWxsKVxuICAucG9zdCh1c2VyQ29udHJvbGxlci5jcmVhdGVPbmUpXG5cbnVzZXJSb3V0ZXJcbiAgLnJvdXRlKCcvOmlkJylcbiAgLmdldCh1c2VyQ29udHJvbGxlci5nZXRPbmUpXG4gIC5wdXQodXNlckNvbnRyb2xsZXIudXBkYXRlT25lKVxuICAucG9zdCh1c2VyQ29udHJvbGxlci5kZWxldGVPbmUpXG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/api/resources/user/user.restRouter.js\n");

/***/ }),

/***/ "./src/api/restRouter.js":
/*!*******************************!*\
  !*** ./src/api/restRouter.js ***!
  \*******************************/
/*! exports provided: restRouter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"restRouter\", function() { return restRouter; });\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _resources_user__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./resources/user */ \"./src/api/resources/user/index.js\");\n/* harmony import */ var _resources_playlist__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./resources/playlist */ \"./src/api/resources/playlist/index.js\");\n/* harmony import */ var _resources_song__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resources/song */ \"./src/api/resources/song/index.js\");\n/* harmony import */ var _modules_errorHandler__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modules/errorHandler */ \"./src/api/modules/errorHandler.js\");\n\n\n\n\n\n\nvar restRouter = express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();\n\nrestRouter.use('/user', _resources_user__WEBPACK_IMPORTED_MODULE_1__[\"userRouter\"]);\nrestRouter.use('/song', _resources_song__WEBPACK_IMPORTED_MODULE_3__[\"songRouter\"]);\nrestRouter.use('/playlist', _resources_playlist__WEBPACK_IMPORTED_MODULE_2__[\"playlistRouter\"]);\nrestRouter.use(_modules_errorHandler__WEBPACK_IMPORTED_MODULE_4__[\"errorHandler\"]);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvYXBpL3Jlc3RSb3V0ZXIuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2FwaS9yZXN0Um91dGVyLmpzPzBkODMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGV4cHJlc3MgZnJvbSAnZXhwcmVzcydcbmltcG9ydCB7IHVzZXJSb3V0ZXIgfSBmcm9tICcuL3Jlc291cmNlcy91c2VyJ1xuaW1wb3J0IHsgcGxheWxpc3RSb3V0ZXIgfSBmcm9tICcuL3Jlc291cmNlcy9wbGF5bGlzdCdcbmltcG9ydCB7IHNvbmdSb3V0ZXIgfSBmcm9tICcuL3Jlc291cmNlcy9zb25nJ1xuaW1wb3J0IHsgZXJyb3JIYW5kbGVyIH0gZnJvbSAnLi9tb2R1bGVzL2Vycm9ySGFuZGxlcidcblxuZXhwb3J0IGNvbnN0IHJlc3RSb3V0ZXIgPSBleHByZXNzLlJvdXRlcigpXG5cbnJlc3RSb3V0ZXIudXNlKCcvdXNlcicsIHVzZXJSb3V0ZXIpXG5yZXN0Um91dGVyLnVzZSgnL3NvbmcnLCBzb25nUm91dGVyKVxucmVzdFJvdXRlci51c2UoJy9wbGF5bGlzdCcsIHBsYXlsaXN0Um91dGVyKVxucmVzdFJvdXRlci51c2UoZXJyb3JIYW5kbGVyKVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/api/restRouter.js\n");

/***/ }),

/***/ "./src/config/dev.js":
/*!***************************!*\
  !*** ./src/config/dev.js ***!
  \***************************/
/*! exports provided: config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"config\", function() { return config; });\nvar config = {\n  expireTime: '30d',\n  secrets: {\n    JWT_SECRET: 'pr3ttyc00lth0'\n  },\n  disableAuth: true\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29uZmlnL2Rldi5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvY29uZmlnL2Rldi5qcz80NmQwIl0sInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBjb25maWcgPSB7XG4gIGV4cGlyZVRpbWU6ICczMGQnLFxuICBzZWNyZXRzOiB7XG4gICAgSldUX1NFQ1JFVDogJ3ByM3R0eWMwMGx0aDAnXG4gIH0sXG4gIGRpc2FibGVBdXRoOiB0cnVlXG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFMQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/config/dev.js\n");

/***/ }),

/***/ "./src/config/index.js":
/*!*****************************!*\
  !*** ./src/config/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash.merge */ \"lodash.merge\");\n/* harmony import */ var lodash_merge__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_merge__WEBPACK_IMPORTED_MODULE_0__);\n\n\nif (false) {}\n\nvar env = \"development\";\n\nvar baseConfig = {\n  port: 3000,\n  secrets: {},\n  db: {\n    url: 'mongodb://localhost/jams'\n  }\n};\n\nvar envConfig = {};\n\nswitch (env) {\n  case 'development':\n  case 'dev':\n    envConfig = __webpack_require__(/*! ./dev */ \"./src/config/dev.js\").config;\n    break;\n  case 'test':\n  case 'testing':\n    envConfig = __webpack_require__(/*! ./testing */ \"./src/config/testing.js\").config;\n    break;\n  case 'prod':\n  case 'production':\n    envConfig = __webpack_require__(/*! ./prod */ \"./src/config/prod.js\").config;\n    break;\n  default:\n    envConfig = __webpack_require__(/*! ./dev */ \"./src/config/dev.js\").config;\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (lodash_merge__WEBPACK_IMPORTED_MODULE_0___default()(baseConfig, envConfig));//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29uZmlnL2luZGV4LmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9jb25maWcvaW5kZXguanM/MWMyNiJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgbWVyZ2UgZnJvbSAnbG9kYXNoLm1lcmdlJ1xuXG5pZiAoIXByb2Nlc3MuZW52Lk5PREVfRU5WKSBwcm9jZXNzLmVudi5OT0RFX0VOViA9ICdkZXZlbG9wbWVudCdcblxuY29uc3QgZW52ID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlZcblxuY29uc3QgYmFzZUNvbmZpZyA9IHtcbiAgcG9ydDogMzAwMCxcbiAgc2VjcmV0czoge30sXG4gIGRiOiB7XG4gICAgdXJsOiAnbW9uZ29kYjovL2xvY2FsaG9zdC9qYW1zJ1xuICB9XG59XG5cbmxldCBlbnZDb25maWcgPSB7fVxuXG5zd2l0Y2ggKGVudikge1xuICBjYXNlICdkZXZlbG9wbWVudCc6XG4gIGNhc2UgJ2Rldic6XG4gICAgZW52Q29uZmlnID0gcmVxdWlyZSgnLi9kZXYnKS5jb25maWdcbiAgICBicmVha1xuICBjYXNlICd0ZXN0JzpcbiAgY2FzZSAndGVzdGluZyc6XG4gICAgZW52Q29uZmlnID0gcmVxdWlyZSgnLi90ZXN0aW5nJykuY29uZmlnXG4gICAgYnJlYWtcbiAgY2FzZSAncHJvZCc6XG4gIGNhc2UgJ3Byb2R1Y3Rpb24nOlxuICAgIGVudkNvbmZpZyA9IHJlcXVpcmUoJy4vcHJvZCcpLmNvbmZpZ1xuICAgIGJyZWFrXG4gIGRlZmF1bHQ6XG4gICAgZW52Q29uZmlnID0gcmVxdWlyZSgnLi9kZXYnKS5jb25maWdcbn1cblxuZXhwb3J0IGRlZmF1bHQgbWVyZ2UoYmFzZUNvbmZpZywgZW52Q29uZmlnKVxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBREE7QUFIQTtBQUNBO0FBT0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWRBO0FBQ0E7QUFnQkEiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/config/index.js\n");

/***/ }),

/***/ "./src/config/prod.js":
/*!****************************!*\
  !*** ./src/config/prod.js ***!
  \****************************/
/*! exports provided: config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"config\", function() { return config; });\nvar config = {};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29uZmlnL3Byb2QuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2NvbmZpZy9wcm9kLmpzP2NmN2EiXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IGNvbmZpZyA9IHt9XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/config/prod.js\n");

/***/ }),

/***/ "./src/config/testing.js":
/*!*******************************!*\
  !*** ./src/config/testing.js ***!
  \*******************************/
/*! exports provided: config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"config\", function() { return config; });\nvar config = {\n  expireTime: '30d',\n  secrets: {\n    JWT_SECRET: 'pr3ttyc00lth0'\n  },\n  db: {\n    url: 'mongodb://localhost/jams-test'\n  },\n  disableAuth: false\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY29uZmlnL3Rlc3RpbmcuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2NvbmZpZy90ZXN0aW5nLmpzP2M4MDciXSwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNvbnN0IGNvbmZpZyA9IHtcbiAgZXhwaXJlVGltZTogJzMwZCcsXG4gIHNlY3JldHM6IHtcbiAgICBKV1RfU0VDUkVUOiAncHIzdHR5YzAwbHRoMCdcbiAgfSxcbiAgZGI6IHtcbiAgICB1cmw6ICdtb25nb2RiOi8vbG9jYWxob3N0L2phbXMtdGVzdCdcbiAgfSxcbiAgZGlzYWJsZUF1dGg6IGZhbHNlXG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQURBO0FBR0E7QUFDQTtBQURBO0FBR0E7QUFSQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/config/testing.js\n");

/***/ }),

/***/ "./src/db.js":
/*!*******************!*\
  !*** ./src/db.js ***!
  \*******************/
/*! exports provided: connect */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"connect\", function() { return connect; });\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mongoose */ \"mongoose\");\n/* harmony import */ var mongoose__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mongoose__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config */ \"./src/config/index.js\");\n\n\n\n\nmongoose__WEBPACK_IMPORTED_MODULE_0___default.a.Promise = global.Promise;\n\nvar connect = function connect() {\n  return mongoose__WEBPACK_IMPORTED_MODULE_0___default.a.connect(_config__WEBPACK_IMPORTED_MODULE_1__[\"default\"].db.url);\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvZGIuanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2RiLmpzPzM5OGEiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IG1vbmdvb3NlIGZyb20gJ21vbmdvb3NlJ1xuXG5pbXBvcnQgY29uZmlnIGZyb20gJy4vY29uZmlnJ1xuXG5tb25nb29zZS5Qcm9taXNlID0gZ2xvYmFsLlByb21pc2VcblxuZXhwb3J0IGNvbnN0IGNvbm5lY3QgPSAoKSA9PiB7XG4gIHJldHVybiBtb25nb29zZS5jb25uZWN0KGNvbmZpZy5kYi51cmwpXG59XG4iXSwibWFwcGluZ3MiOiJBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/db.js\n");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _server__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./server */ \"./src/server.js\");\n/* harmony import */ var _config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./config */ \"./src/config/index.js\");\n\n\n\n\nvar PORT = _config__WEBPACK_IMPORTED_MODULE_1__[\"default\"].port;\n\n_server__WEBPACK_IMPORTED_MODULE_0__[\"default\"].listen(PORT, function () {\n  console.log('I\\'m listening on port ' + PORT);\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvaW5kZXguanMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vc3JjL2luZGV4LmpzPzEyZDUiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGFwcCBmcm9tICcuL3NlcnZlcidcblxuaW1wb3J0IGNvbmZpZyBmcm9tICcuL2NvbmZpZydcblxuY29uc3QgUE9SVCA9IGNvbmZpZy5wb3J0XG5cbmFwcC5saXN0ZW4oUE9SVCwgKCkgPT4ge1xuICBjb25zb2xlLmxvZyhgSSdtIGxpc3RlbmluZyBvbiBwb3J0ICR7UE9SVH1gKVxufSlcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/index.js\n");

/***/ }),

/***/ "./src/middleware.js":
/*!***************************!*\
  !*** ./src/middleware.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! body-parser */ \"body-parser\");\n/* harmony import */ var body_parser__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(body_parser__WEBPACK_IMPORTED_MODULE_0__);\n\n\nvar setupMidware = function setupMidware(app) {\n  app.use(body_parser__WEBPACK_IMPORTED_MODULE_0___default.a.urlencoded({ extended: true }));\n  app.use(body_parser__WEBPACK_IMPORTED_MODULE_0___default.a.json());\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (setupMidware);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvbWlkZGxld2FyZS5qcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9zcmMvbWlkZGxld2FyZS5qcz81YTczIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBib2R5UGFyc2VyIGZyb20gJ2JvZHktcGFyc2VyJ1xuXG5jb25zdCBzZXR1cE1pZHdhcmUgPSBhcHAgPT4ge1xuICBhcHAudXNlKGJvZHlQYXJzZXIudXJsZW5jb2RlZCh7IGV4dGVuZGVkOiB0cnVlIH0pKVxuICBhcHAudXNlKGJvZHlQYXJzZXIuanNvbigpKVxufVxuXG5leHBvcnQgZGVmYXVsdCBzZXR1cE1pZHdhcmVcbiJdLCJtYXBwaW5ncyI6IkFBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/middleware.js\n");

/***/ }),

/***/ "./src/server.js":
/*!***********************!*\
  !*** ./src/server.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ \"express\");\n/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./api */ \"./src/api/index.js\");\n/* harmony import */ var _middleware__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./middleware */ \"./src/middleware.js\");\n/* harmony import */ var _db__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./db */ \"./src/db.js\");\n/* harmony import */ var _api_modules_auth__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./api/modules/auth */ \"./src/api/modules/auth.js\");\n/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-server-express */ \"apollo-server-express\");\n/* harmony import */ var apollo_server_express__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(apollo_server_express__WEBPACK_IMPORTED_MODULE_5__);\n\n\n\n\n\n\n\n\nvar app = express__WEBPACK_IMPORTED_MODULE_0___default()();\n\n// connect to the db\nObject(_db__WEBPACK_IMPORTED_MODULE_3__[\"connect\"])();\n// setup middleware for global app\nObject(_middleware__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(app);\n\n// mount signin middleware to /signin route\napp.use('/signin', _api_modules_auth__WEBPACK_IMPORTED_MODULE_4__[\"signin\"]);\n\n// mount restRouter route to /api\napp.use('/api', _api_modules_auth__WEBPACK_IMPORTED_MODULE_4__[\"protect\"], _api__WEBPACK_IMPORTED_MODULE_1__[\"restRouter\"]);\n\napp.use('/graphql', _api_modules_auth__WEBPACK_IMPORTED_MODULE_4__[\"protect\"], _api__WEBPACK_IMPORTED_MODULE_1__[\"graphQLRouter\"]);\napp.use('/docs', Object(apollo_server_express__WEBPACK_IMPORTED_MODULE_5__[\"graphiqlExpress\"])({ endpointURL: '/graphql' }));\n\napp.all('*', function (req, res) {\n  res.sendStatus(404).json({ message: 'Not found!' });\n});\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (app);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvc2VydmVyLmpzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL3NyYy9zZXJ2ZXIuanM/OGZmYyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgZXhwcmVzcyBmcm9tICdleHByZXNzJ1xuXG5pbXBvcnQgeyByZXN0Um91dGVyLCBncmFwaFFMUm91dGVyIH0gZnJvbSAnLi9hcGknXG5pbXBvcnQgc2V0dXBNaWR3YXJlIGZyb20gJy4vbWlkZGxld2FyZSdcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICcuL2RiJ1xuaW1wb3J0IHsgcHJvdGVjdCwgc2lnbmluIH0gZnJvbSAnLi9hcGkvbW9kdWxlcy9hdXRoJ1xuaW1wb3J0IHsgZ3JhcGhpcWxFeHByZXNzIH0gZnJvbSAnYXBvbGxvLXNlcnZlci1leHByZXNzJ1xuXG5jb25zdCBhcHAgPSBleHByZXNzKClcblxuLy8gY29ubmVjdCB0byB0aGUgZGJcbmNvbm5lY3QoKVxuLy8gc2V0dXAgbWlkZGxld2FyZSBmb3IgZ2xvYmFsIGFwcFxuc2V0dXBNaWR3YXJlKGFwcClcblxuLy8gbW91bnQgc2lnbmluIG1pZGRsZXdhcmUgdG8gL3NpZ25pbiByb3V0ZVxuYXBwLnVzZSgnL3NpZ25pbicsIHNpZ25pbilcblxuLy8gbW91bnQgcmVzdFJvdXRlciByb3V0ZSB0byAvYXBpXG5hcHAudXNlKCcvYXBpJywgcHJvdGVjdCwgcmVzdFJvdXRlcilcblxuYXBwLnVzZSgnL2dyYXBocWwnLCBwcm90ZWN0LCBncmFwaFFMUm91dGVyKVxuYXBwLnVzZSgnL2RvY3MnLCBncmFwaGlxbEV4cHJlc3MoeyBlbmRwb2ludFVSTDogJy9ncmFwaHFsJyB9KSlcblxuYXBwLmFsbCgnKicsIChyZXEsIHJlcykgPT4ge1xuICByZXMuc2VuZFN0YXR1cyg0MDQpLmpzb24oeyBtZXNzYWdlOiAnTm90IGZvdW5kIScgfSlcbn0pXG5cbmV4cG9ydCBkZWZhdWx0IGFwcFxuIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/server.js\n");

/***/ }),

/***/ "apollo-server-express":
/*!****************************************!*\
  !*** external "apollo-server-express" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"apollo-server-express\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBvbGxvLXNlcnZlci1leHByZXNzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYXBvbGxvLXNlcnZlci1leHByZXNzXCI/MjJmMyJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJhcG9sbG8tc2VydmVyLWV4cHJlc3NcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///apollo-server-express\n");

/***/ }),

/***/ "babel-runtime/core-js/promise":
/*!************************************************!*\
  !*** external "babel-runtime/core-js/promise" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-runtime/core-js/promise\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFiZWwtcnVudGltZS9jb3JlLWpzL3Byb21pc2UuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJiYWJlbC1ydW50aW1lL2NvcmUtanMvcHJvbWlzZVwiPzdlNDQiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYmFiZWwtcnVudGltZS9jb3JlLWpzL3Byb21pc2VcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///babel-runtime/core-js/promise\n");

/***/ }),

/***/ "babel-runtime/helpers/asyncToGenerator":
/*!*********************************************************!*\
  !*** external "babel-runtime/helpers/asyncToGenerator" ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-runtime/helpers/asyncToGenerator\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFiZWwtcnVudGltZS9oZWxwZXJzL2FzeW5jVG9HZW5lcmF0b3IuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJiYWJlbC1ydW50aW1lL2hlbHBlcnMvYXN5bmNUb0dlbmVyYXRvclwiP2E2N2EiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYmFiZWwtcnVudGltZS9oZWxwZXJzL2FzeW5jVG9HZW5lcmF0b3JcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///babel-runtime/helpers/asyncToGenerator\n");

/***/ }),

/***/ "babel-runtime/helpers/extends":
/*!************************************************!*\
  !*** external "babel-runtime/helpers/extends" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-runtime/helpers/extends\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kc1wiPzM2ZDkiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHNcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///babel-runtime/helpers/extends\n");

/***/ }),

/***/ "babel-runtime/helpers/objectWithoutProperties":
/*!****************************************************************!*\
  !*** external "babel-runtime/helpers/objectWithoutProperties" ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-runtime/helpers/objectWithoutProperties\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzXCI/YzdlOSJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXNcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///babel-runtime/helpers/objectWithoutProperties\n");

/***/ }),

/***/ "babel-runtime/regenerator":
/*!********************************************!*\
  !*** external "babel-runtime/regenerator" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"babel-runtime/regenerator\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFiZWwtcnVudGltZS9yZWdlbmVyYXRvci5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9leHRlcm5hbCBcImJhYmVsLXJ1bnRpbWUvcmVnZW5lcmF0b3JcIj8wOWNjIl0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImJhYmVsLXJ1bnRpbWUvcmVnZW5lcmF0b3JcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///babel-runtime/regenerator\n");

/***/ }),

/***/ "bcrypt":
/*!*************************!*\
  !*** external "bcrypt" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"bcrypt\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmNyeXB0LmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYmNyeXB0XCI/Y2Y5YyJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJiY3J5cHRcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///bcrypt\n");

/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"body-parser\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9keS1wYXJzZXIuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJib2R5LXBhcnNlclwiPzgxODgiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiYm9keS1wYXJzZXJcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///body-parser\n");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwcmVzcy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9leHRlcm5hbCBcImV4cHJlc3NcIj8yMmZlIl0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImV4cHJlc3NcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///express\n");

/***/ }),

/***/ "express-jwt":
/*!******************************!*\
  !*** external "express-jwt" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"express-jwt\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXhwcmVzcy1qd3QuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJleHByZXNzLWp3dFwiPzNjZDEiXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZXhwcmVzcy1qd3RcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///express-jwt\n");

/***/ }),

/***/ "graphql-tools":
/*!********************************!*\
  !*** external "graphql-tools" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"graphql-tools\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JhcGhxbC10b29scy5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9leHRlcm5hbCBcImdyYXBocWwtdG9vbHNcIj8yNGNhIl0sInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcImdyYXBocWwtdG9vbHNcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///graphql-tools\n");

/***/ }),

/***/ "jsonwebtoken":
/*!*******************************!*\
  !*** external "jsonwebtoken" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"jsonwebtoken\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbndlYnRva2VuLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwianNvbndlYnRva2VuXCI/NjQ5MCJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJqc29ud2VidG9rZW5cIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///jsonwebtoken\n");

/***/ }),

/***/ "lodash.merge":
/*!*******************************!*\
  !*** external "lodash.merge" ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"lodash.merge\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9kYXNoLm1lcmdlLmpzIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibG9kYXNoLm1lcmdlXCI/ZTU1MiJdLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJsb2Rhc2gubWVyZ2VcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///lodash.merge\n");

/***/ }),

/***/ "mongoose":
/*!***************************!*\
  !*** external "mongoose" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = require(\"mongoose\");//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9uZ29vc2UuanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJtb25nb29zZVwiP2ZmZDciXSwic291cmNlc0NvbnRlbnQiOlsibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibW9uZ29vc2VcIik7Il0sIm1hcHBpbmdzIjoiQUFBQSIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///mongoose\n");

/***/ })

/******/ });